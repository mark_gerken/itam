import React from 'react';
import {
  createMuiTheme,
  ThemeProvider,
  CssBaseline
} from '@material-ui/core';
import Fleet from './app/components/Fleet';
import Machine from './app/components/Machine';
import { makeStyles } from '@material-ui/core/styles';
import { Route } from 'react-router';
import { BrowserRouter, Redirect } from 'react-router-dom';

const useStyles = makeStyles({
  root: {
    marginLeft: 30,
    marginRight: 30
  }
});

const theme = createMuiTheme({
  palette: {
    type: 'dark',
    // primary: {
    //   light: '#5e92f3',
    //   main: '#1565c0',
    //   dark: '#003c8f',
    //   contrastText: '#fff',
    // },
    // secondary: {
    //   light: '#c158dc',
    //   main: '#8e24aa',
    //   dark: '#5c007a',
    //   contrastText: '#ffffff',
    // }
  }
});

function App() {
  const classes = useStyles();

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <div className={classes.root}>
        <BrowserRouter>
          <Route exact path="/"><Redirect to="/fleet" /></Route>
          <Route exact path="/machine"><Redirect to="/fleet" /></Route>
          <Route path="/fleet" component={Fleet} />
          <Route path="/machine/:name/:tab?/:run?" component={Machine} />
        </BrowserRouter>
      </div>
    </ThemeProvider>
  );
}

export default App;
