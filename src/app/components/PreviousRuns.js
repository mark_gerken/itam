import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InspecScan from './InspecScan';
import { Link } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    root: {
        margin: 10,
        padding: 10,
        display: 'flex',
        flexDirection: 'column'
    },
    previousRunRoot: {
    },
    tabLink: {
        color: 'inherit'
    },
    previousRunTitle: {
        padding: 10,
        background: theme.palette.primary.dark,
        borderRadius: 10
    },
    previousRunTitleFailed: {
        padding: 10,
        background: theme.palette.secondary.dark,
        borderRadius: 10
    },
    previousRunCollapsed: {
        display: 'none'
    },
    previousRunExpanded: {
        background: '#333333'
    }
}));

function PreviousRuns({selectedRun, machineName, runs}) {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            {
                runs.map((run, index) => {
                    let success = true;
                    run.controls.forEach((control) => {
                        if (control.status != 'passed') {
                            success = false;
                        }
                    });
                    const runIsSelected = selectedRun == index;
                    return (
                        <div className={classes.previousRunRoot}>
                            <Link className={classes.tabLink} to={runIsSelected ? `/machine/${machineName}/previous_runs` : `/machine/${machineName}/previous_runs/${index}`}>
                                <div className={success ? classes.previousRunTitle : classes.previousRunTitleFailed}>
                                    {"#" + index + " :: Date: " + run.date}
                                </div>
                            </Link>
                            <div className={runIsSelected ? classes.previousRunExpanded : classes.previousRunCollapsed}><InspecScan scan={run}/></div>
                        </div>
                    )
                })
            }
        </div>
    )
}

export default PreviousRuns;
