import React from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import { useLocation } from "react-router-dom";
import {
  Paper
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: 10,
        padding: 10,
        marginBottom: 10,
        borderRadius: 10
    },
    breadcrumbs: {
        color: 'textPrimary'
    },
    breadcrumbLink: {
        color: 'inherit'
    },
    breadcrumbEnd: {
        color: 'white'
    }
}));

function CustomBreadcrumbs() {
    const classes = useStyles();
    const location = useLocation();
    let path = location.pathname;
    if (path[path.length - 1] === '/') {
        path = path.slice(0, -1)
    }

    return (
        <Paper className={classes.root}>
            <Breadcrumbs className={classes.breadcrumbs} aria-label="breadcrumb">
                <Link className={classes.breadcrumbLink} to="/">Home</Link>
                {
                    path.split('/').slice(1).map((pathPortion, index) => {
                        if (index === path.split('/').slice(1).length - 1) {
                            return <Typography key={index} className={classes.breadcrumbEnd}>{ pathPortion }</Typography>
                        } else {
                            return <Link key={index} className={classes.breadcrumbLink} to={path.split('/').slice(0, index + 2).join('/')}>
                                { pathPortion }
                            </Link>
                        }
                    })
                }
            </Breadcrumbs>
        </Paper>
    );
}

export default CustomBreadcrumbs;
