import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InspecTest from './InspecTest';

const useStyles = makeStyles(theme => ({
    root: {
        margin: 10,
        padding: 10,
        display: 'flex',
        flexDirection: 'column'
    },
    summary: {
        padding: 10,
        background: '#222222'
    }
}));

function Summary({scan}) {
    const classes = useStyles();
    let success = true;
    scan.controls.forEach((control) => {
        if (control.status != 'passed') {
            success = false;
        }
    });
    return (
        <div className={classes.summary}>
            { success ? 'COMPLIANT' : 'NON-COMPLIANT' }
        </div>
    )
}

function InspecScan({scan}) {
    const classes = useStyles();
    return (
        <div>
            {
                scan ? <Summary scan={scan}></Summary> : ""
            }
            <div className={classes.root}>
                {
                    scan?.controls?.map((control, index) => <InspecTest key={index} test={control}></InspecTest>)
                }
            </div>
        </div>
    );
}

export default InspecScan;
