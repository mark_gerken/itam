import React from 'react';
import {
    Paper
} from '@material-ui/core';
import {
    useParams,
} from 'react-router-dom';
import { loadMachine } from '../redux/actions/api_actions';
import { useEffect } from 'react';
import { useSelector, connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import CustomBreadcrumbs from './CustomBreadCrumbs';
import ReactJson from 'react-json-view'
import AppBar from '@material-ui/core/AppBar';
import PreviousRuns from './PreviousRuns';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import InspecScan from './InspecScan';
import { Link } from 'react-router-dom';
import LinearProgress from '@material-ui/core/LinearProgress';

const useStyles = makeStyles(theme => ({
    root: {
        margin: 10,
        padding: 10,
        display: 'flex',
        flexDirection: 'column'
    },
    tabLink: {
        color: 'inherit'
    },
    content: {
        borderRadius: 10,
        overflow: 'hidden'
    },
    loading: {
        display: 'flex',
        flexDirection: 'column'
    }
}));

function Machine(store) {
    const { name, tab, run } = useParams();

    let value;
    if (!tab) {
        value = 0;
    } else if (tab === 'previous_runs') {
        value = 1;
    } else {
        value = 2;
    }

    useEffect(() => {
        store.dispatch(loadMachine(name));
    }, [name]);

    const machineDetails = useSelector((store) => {
        return store.machine;
    });

    const classes = useStyles();

    return (
        <div className={classes.root}>
            <CustomBreadcrumbs />

            <Paper className={classes.content}>
                <AppBar position="static">
                    <Tabs value={value} aria-label="">
                        <Link className={classes.tabLink} to={`/machine/${name}/`}><Tab label="Most Recent Scan"/></Link>
                        <Link className={classes.tabLink} to={`/machine/${name}/previous_runs`}><Tab label="Previous Runs"/></Link>
                        <Link className={classes.tabLink} to={`/machine/${name}/details`}><Tab label="Details"/></Link>
                    </Tabs>
                </AppBar>
                <div className={classes.subContent}>
                    {
                        machineDetails.name === null ?
                            <div>
                                <LinearProgress color="secondary" />
                                <center>loading...</center>
                            </div> 
                        : tab === 'details' ?
                            <ReactJson collapsed='true' theme='colors' src={machineDetails.details} />
                        : tab === 'previous_runs' ?
                            <PreviousRuns selectedRun={run} machineName={name} runs={machineDetails.inspecRuns}/>
                        :   <InspecScan scan={machineDetails.inspecRuns[0]}/>
                    }
                </div>
            </Paper>
        </div>
    );
}

export default connect()(Machine);
