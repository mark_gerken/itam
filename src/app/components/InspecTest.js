import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        borderRadius: 10,
        margin: 10
    },
    failedTest: {
        padding: 10,
        borderRadius: 10,
        background: theme.palette.secondary.dark
    },
    testPassed: {
        padding: 10,
        background: theme.palette.primary.main,
        borderRadius: 10,
        // border: '1 solid black'
    },
    hidden: {
        display: 'none'
    },
    expanded: {
        padding: 10,
        background: '#222222',
        display: 'flex',
        flexDirection: 'column'
    }
}));

function InspecTest({test}) {
    const classes = useStyles();
    
    const [expanded, setExpanded] = useState(false);

    return (
        <div className={classes.root}>
            <div className={test.status === 'passed' ? classes.testPassed : classes.failedTest} onClick={() => setExpanded(!expanded)}>
                {test.profile_id} :: {test.id}
            </div>
            <div className={expanded ? classes.expanded : classes.hidden}>
                <div>{"status:\t " + test.status }</div>
                <div>{"message:\t " + test.message }</div>
            </div>
        </div>
    );
}

export default InspecTest;
