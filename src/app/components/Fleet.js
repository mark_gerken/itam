import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { loadFleet, triggerScan, scanAllMachines } from '../redux/actions/api_actions';
import { useSelector, connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import CustomBreadcrumbs from './CustomBreadCrumbs';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles(theme => ({
    root: {
        margin: 10,
        padding: 10,
        display: 'flex',
        flexDirection: 'column'
    },
    searchBar: {
        marginTop: 5,
        marginBottom: 5,
        display: 'flex',
        flexDirection: 'row'
    },
    searchField: {
        flexGrow: 0,
        width: '20%',
        background: theme.palette.background.paper,
        borderRadius: 10,
        border: 'none',
        color: 'white',
        padding: 10
    },
    spacer: {
        flexGrow: 1
    },
    content: {
        display: 'flex',
        flexDirection: 'row'
    },
    card: {
        flexGrow: 1,
        minWidth: 200,
        maxWidth: 400,
        marginRight: 20,
        marginBottom: 20,
        display: 'flex',
        flexDirection: 'column',
        borderRadius: 10,
        boxShadow: '5px 5px 0px 0px rgba(0, 0, 0, .3)',
    },
    cardHeader: {
        padding: 0,
        flexGrow: 0
    },
    cardContent: {
        flexGrow: 1,
        fontSize: 14
    },
    cardFooter: {
        flexGrow: 0,
        display: 'flex',
        flexDirection: 'row'
    },
    titleFailed: {
        background: theme.palette.secondary.dark,
        padding: 15,
        fontSize: 25
    },
    title: {
        background: theme.palette.primary.dark,
        padding: 15,
        fontSize: 25
    },
    loading: {
        flexGrow: 1,
        display: 'flex',
        flexDirection: 'column'
    },
    buttonLoading: {
        marginRight: 10
    }
}));

function Fleet(store) {
    console.log('render fleet');
    useEffect(() => {
        store.dispatch(loadFleet);
    }, []);

    let originalFleet = useSelector((store) => {
        return store.fleet;
    });

    const activeScans = useSelector((store) => {
        return store.activeScans;
    });

    let loading = false;
    if (originalFleet === null) {
        loading = true;
        originalFleet = [];
    }

    const [fleet, setFleet] = useState([]);

    useEffect(() => {
        setFleet(originalFleet);
    }, [originalFleet]);

    const classes = useStyles();

    const searchNodes = () => {
        let currentTimeout = null;
        return (event) => {
            if (currentTimeout != null) {
                clearTimeout(currentTimeout);
            }
            currentTimeout = setTimeout(
                () => {
                    setFleet(originalFleet.filter((machine) => {
                        return JSON.stringify(machine).indexOf(event.target.value) >= 0;
                    }));
                },
                1000
            );
        }
    }

    return (
        <div className={classes.root}>
            <CustomBreadcrumbs />
            <div className={classes.searchBar}>
                <Button size="small" onClick={() => store.dispatch(scanAllMachines)}>Scan Fleet</Button>
                <div className={classes.spacer}></div>
                <input className={classes.searchField} type="text" placeholder="Search..." onChange={searchNodes()} />
            </div>
            <div className={classes.content}>
                {
                    loading ?
                        <div className={classes.loading}><center><CircularProgress size={80} color="secondary" /></center></div>
                    : fleet.map((machine, index) => <Card key={index} className={classes.card}>
                        <CardContent className={classes.cardHeader}>
                            <Link to={"/machine/" + machine.name}>
                                <Typography className={machine.latest_inspec_run?.result === 'noncompliant' ? classes.titleFailed : classes.title} color="textSecondary" gutterBottom>
                                    {machine.name}
                                </Typography>
                            </Link>
                        </CardContent>
                        <CardContent className={classes.cardContent}>
                            <Typography color="textSecondary">
                                Status: { machine.latest_inspec_run === null ? "UNKNOWN" : machine.latest_inspec_run.result }
                            </Typography>
                            <Typography color="textSecondary">
                                Known issues: {machine.latest_inspec_run?.issues || '?'}
                            </Typography>
                        </CardContent>
                        <CardActions className={classes.cardFooter}>
                            {
                                activeScans[machine.name] ?
                                    <Button size="small"><CircularProgress className={classes.buttonLoading} size={20} color="secondary" />Scanning...</Button>
                                :   <Button size="small" onClick={() => store.dispatch(triggerScan(machine.name))}>Scan</Button>
                            }
                            <div className={classes.spacer}></div>
                            <Link to={"/machine/" + machine.name}>
                                <Button size="small">View Details</Button>
                            </Link>
                        </CardActions>
                    </Card>)}
            </div>
        </div>
    );
}

export default connect()(Fleet);
