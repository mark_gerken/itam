const initialState = {
    name: null,
    details: null,
    inspecRuns: [],
    compliant: true
};

export default function machineReducer(state = initialState, action) {
    switch(action.type) {
        case 'LOAD_MACHINE': {
            return {...initialState, ...action.value};
        }
        case 'UNLOAD_MACHINE': {
            return initialState;
        }
        default: {
            return state;
        }
    }
}