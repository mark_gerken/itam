const initialState = {};

export default function activeScaneReducer(state = initialState, action) {
    switch(action.type) {
        case 'SCAN_INITIATED': {
            return {...state, [action.value]: true}
        }
        case 'SCAN_COMPLETE': {
            let newState = {...state}
            delete newState[action.value];
            return newState;
        }
        default: {
            return state;
        }
    }
}