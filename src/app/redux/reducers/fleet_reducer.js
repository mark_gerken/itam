const initialState = null;

export default function fleetReducer(state = initialState, action) {
    switch(action.type) {
        case 'LOAD_FLEET': {
            return action.value;
        }
        default: {
            return state;
        }
    }
}