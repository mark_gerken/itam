export const nexus_inspec = {
    "controls": [
        {
            "id": "jdk:path:version",
            "profile_id": "jdk",
            "profile_sha256": "16217013e5791192227f6cb5a0bb417c088f033a3bec275d72e47a1451acc3fe",
            "status": "failed",
            "code_desc": "shell success is expected to eq false",
            "message": "\nexpected: false\n     got: true\n\n(compared using ==)\n\nDiff:\n@@ -1,2 +1,2 @@\n-false\n+true\n",
            "exception": "RSpec::Core::MultipleExceptionError"
        },
        {
            "id": "jdk:path:version",
            "profile_id": "jdk",
            "profile_sha256": "16217013e5791192227f6cb5a0bb417c088f033a3bec275d72e47a1451acc3fe",
            "status": "passed",
            "code_desc": "shell success is expected to eq true"
        },
        {
            "id": "jdk:path:version",
            "profile_id": "jdk",
            "profile_sha256": "16217013e5791192227f6cb5a0bb417c088f033a3bec275d72e47a1451acc3fe",
            "status": "failed",
            "code_desc": "shell value is expected to include \"11.0.11\"",
            "message": "expected \"openjdk version \\\"1.8.0_292\\\"\\nOpenJDK Runtime Environment (build 1.8.0_292-8u292-b10-0ubuntu1~20.04-b10)\\nOpenJDK 64-Bit Server VM (build 25.292-b10, mixed mode)\\n\" to include \"11.0.11\"\nDiff:\n@@ -1,2 +1,4 @@\n-11.0.11\n+openjdk version \"1.8.0_292\"\n+OpenJDK Runtime Environment (build 1.8.0_292-8u292-b10-0ubuntu1~20.04-b10)\n+OpenJDK 64-Bit Server VM (build 25.292-b10, mixed mode)\n",
            "exception": "RSpec::Core::MultipleExceptionError"
        }
    ],
    "statistics": {
        "duration": 0.002419102
    },
    "version": "4.37.20"
}