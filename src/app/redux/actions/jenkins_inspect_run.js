export const jenkins_inspec = {
    "controls": [
        {
            "id": "jdk:path:version",
            "profile_id": "jdk",
            "profile_sha256": "16217013e5791192227f6cb5a0bb417c088f033a3bec275d72e47a1451acc3fe",
            "status": "passed",
            "code_desc": "shell success is expected to eq true"
        },
        {
            "id": "jdk:path:version",
            "profile_id": "jdk",
            "profile_sha256": "16217013e5791192227f6cb5a0bb417c088f033a3bec275d72e47a1451acc3fe",
            "status": "passed",
            "code_desc": "shell value is expected to include \"11.0.11\""
        }
    ],
    "statistics": {
        "duration": 0.001939327
    },
    "version": "4.37.20"
}