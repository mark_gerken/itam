export const nexus_raw = {
  "name": "jenkins",
  "chef_environment": "_default",
  "json_class": "Chef::Node",
  "automatic": {
    "virtualization": {
      "systems": {
        "vbox": "host",
        "kvm": "host",
        "docker": "guest"
      },
      "system": "docker",
      "role": "guest"
    },
    "kernel": {
      "name": "Linux",
      "release": "5.8.0-53-generic",
      "version": "#60~20.04.1-Ubuntu SMP Thu May 6 09:52:46 UTC 2021",
      "machine": "x86_64",
      "processor": "x86_64",
      "os": "GNU/Linux",
      "modules": {

      }
    },
    "network": {
      "interfaces": {

      }
    },
    "counters": {
      "network": {

      }
    },
    "filesystem": {
      "by_device": {
        "overlay": {
          "kb_size": "490691512",
          "kb_used": "98299536",
          "kb_available": "367396496",
          "percent_used": "22%",
          "total_inodes": "31227904",
          "inodes_used": "1582051",
          "inodes_available": "29645853",
          "inodes_percent_used": "6%",
          "fs_type": "overlay",
          "mount_options": [
            "rw",
            "relatime",
            "lowerdir=/var/lib/docker/overlay2/l/6SHGEXVZPDKCO7WUU6IDNJXYXS:/var/lib/docker/overlay2/l/YO3CCII6IESV7DRDND7KBM6QGB:/var/lib/docker/overlay2/l/LWRRY7A33VLU4ISZ5AEX2OUBJ2:/var/lib/docker/overlay2/l/NZXYLZOKAFYWESVSBUHRZISN4F:/var/lib/docker/overlay2/l/K2ZYSGV74DW4RNH2BTKYM6BH2X:/var/lib/docker/overlay2/l/PG2ZTNFPPPQIX4NBVUDJA3XUXA:/var/lib/docker/overlay2/l/XSDYFDIGGDNG75BIDUYOX5RHJD:/var/lib/docker/overlay2/l/JMS47CCCE5CVS5AZSDJQ7QFZQT:/var/lib/docker/overlay2/l/G7QPRQ3QUQWRBNRB7PNSX2VPEM:/var/lib/docker/overlay2/l/6ZTQCWYCBLUOUHLIQC3BNYXTYZ:/var/lib/docker/overlay2/l/5HJOTERUPUHWNMASDCWPQW5JD3:/var/lib/docker/overlay2/l/75YFVNFSF7XJDZHOHCS7NHI5R3:/var/lib/docker/overlay2/l/YBEE3POARI5YV326AVMRBYYRYD:/var/lib/docker/overlay2/l/R3DBMMCSVLOQAQGQP7DP5HKJ4Y",
            "upperdir=/var/lib/docker/overlay2/89856b1763c31d022f5161d42069bf069deaaeea98a5c0397069b7a5c064d922/diff",
            "workdir=/var/lib/docker/overlay2/89856b1763c31d022f5161d42069bf069deaaeea98a5c0397069b7a5c064d922/work"
          ],
          "mounts": [
            "/"
          ]
        },
        "tmpfs": {
          "kb_size": "1629320",
          "kb_used": "2552",
          "kb_available": "1626768",
          "percent_used": "1%",
          "total_inodes": "2036645",
          "inodes_used": "1325",
          "inodes_available": "2035320",
          "inodes_percent_used": "1%",
          "fs_type": "tmpfs",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "size=1629320k",
            "mode=755"
          ],
          "mounts": [
            "/dev",
            "/sys/fs/cgroup",
            "/run/docker.sock"
          ]
        },
        "shm": {
          "kb_size": "65536",
          "kb_used": "0",
          "kb_available": "65536",
          "percent_used": "0%",
          "total_inodes": "2036645",
          "inodes_used": "1",
          "inodes_available": "2036644",
          "inodes_percent_used": "1%",
          "fs_type": "tmpfs",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "size=65536k"
          ],
          "mounts": [
            "/dev/shm"
          ]
        },
        "/dev/nvme0n1p2": {
          "kb_size": "490691512",
          "kb_used": "98299536",
          "kb_available": "367396496",
          "percent_used": "22%",
          "total_inodes": "31227904",
          "inodes_used": "1582051",
          "inodes_available": "29645853",
          "inodes_percent_used": "6%",
          "fs_type": "ext4",
          "mount_options": [
            "rw",
            "relatime",
            "errors=remount-ro"
          ],
          "uuid": "aa041608-24c8-4ad6-8941-eba955f28642",
          "mounts": [
            "/root/.chef",
            "/etc/resolv.conf",
            "/etc/hostname",
            "/etc/hosts",
            "/var/lib/jenkins"
          ]
        },
        "proc": {
          "fs_type": "proc",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime"
          ],
          "mounts": [
            "/proc"
          ]
        },
        "devpts": {
          "fs_type": "devpts",
          "mount_options": [
            "rw",
            "nosuid",
            "noexec",
            "relatime",
            "gid=5",
            "mode=620",
            "ptmxmode=666"
          ],
          "mounts": [
            "/dev/pts"
          ]
        },
        "sysfs": {
          "fs_type": "sysfs",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime"
          ],
          "mounts": [
            "/sys"
          ]
        },
        "cgroup": {
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "cpu",
            "cpuacct"
          ],
          "mounts": [
            "/sys/fs/cgroup/systemd",
            "/sys/fs/cgroup/freezer",
            "/sys/fs/cgroup/memory",
            "/sys/fs/cgroup/net_cls,net_prio",
            "/sys/fs/cgroup/perf_event",
            "/sys/fs/cgroup/cpuset",
            "/sys/fs/cgroup/blkio",
            "/sys/fs/cgroup/pids",
            "/sys/fs/cgroup/rdma",
            "/sys/fs/cgroup/hugetlb",
            "/sys/fs/cgroup/devices",
            "/sys/fs/cgroup/cpu,cpuacct"
          ]
        },
        "mqueue": {
          "fs_type": "mqueue",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime"
          ],
          "mounts": [
            "/dev/mqueue"
          ]
        },
        "/dev/loop0": {
          "fs_type": "squashfs",
          "mounts": [

          ]
        },
        "/dev/loop1": {
          "fs_type": "squashfs",
          "mounts": [

          ]
        },
        "/dev/loop2": {
          "fs_type": "squashfs",
          "mounts": [

          ]
        },
        "/dev/loop3": {
          "fs_type": "squashfs",
          "mounts": [

          ]
        },
        "/dev/loop4": {
          "fs_type": "squashfs",
          "mounts": [

          ]
        },
        "/dev/loop5": {
          "fs_type": "squashfs",
          "mounts": [

          ]
        },
        "/dev/loop6": {
          "fs_type": "squashfs",
          "mounts": [

          ]
        },
        "/dev/loop7": {
          "fs_type": "squashfs",
          "mounts": [

          ]
        },
        "/dev/loop8": {
          "fs_type": "squashfs",
          "mounts": [

          ]
        },
        "/dev/loop9": {
          "fs_type": "squashfs",
          "mounts": [

          ]
        },
        "/dev/loop10": {
          "fs_type": "squashfs",
          "mounts": [

          ]
        },
        "/dev/loop11": {
          "fs_type": "squashfs",
          "mounts": [

          ]
        },
        "/dev/loop12": {
          "fs_type": "squashfs",
          "mounts": [

          ]
        },
        "/dev/nvme0n1": {
          "mounts": [

          ]
        },
        "/dev/nvme0n1p1": {
          "fs_type": "vfat",
          "uuid": "BE71-571F",
          "mounts": [

          ]
        }
      },
      "by_mountpoint": {
        "/": {
          "kb_size": "490691512",
          "kb_used": "98299536",
          "kb_available": "367396496",
          "percent_used": "22%",
          "total_inodes": "31227904",
          "inodes_used": "1582051",
          "inodes_available": "29645853",
          "inodes_percent_used": "6%",
          "fs_type": "overlay",
          "mount_options": [
            "rw",
            "relatime",
            "lowerdir=/var/lib/docker/overlay2/l/6SHGEXVZPDKCO7WUU6IDNJXYXS:/var/lib/docker/overlay2/l/YO3CCII6IESV7DRDND7KBM6QGB:/var/lib/docker/overlay2/l/LWRRY7A33VLU4ISZ5AEX2OUBJ2:/var/lib/docker/overlay2/l/NZXYLZOKAFYWESVSBUHRZISN4F:/var/lib/docker/overlay2/l/K2ZYSGV74DW4RNH2BTKYM6BH2X:/var/lib/docker/overlay2/l/PG2ZTNFPPPQIX4NBVUDJA3XUXA:/var/lib/docker/overlay2/l/XSDYFDIGGDNG75BIDUYOX5RHJD:/var/lib/docker/overlay2/l/JMS47CCCE5CVS5AZSDJQ7QFZQT:/var/lib/docker/overlay2/l/G7QPRQ3QUQWRBNRB7PNSX2VPEM:/var/lib/docker/overlay2/l/6ZTQCWYCBLUOUHLIQC3BNYXTYZ:/var/lib/docker/overlay2/l/5HJOTERUPUHWNMASDCWPQW5JD3:/var/lib/docker/overlay2/l/75YFVNFSF7XJDZHOHCS7NHI5R3:/var/lib/docker/overlay2/l/YBEE3POARI5YV326AVMRBYYRYD:/var/lib/docker/overlay2/l/R3DBMMCSVLOQAQGQP7DP5HKJ4Y",
            "upperdir=/var/lib/docker/overlay2/89856b1763c31d022f5161d42069bf069deaaeea98a5c0397069b7a5c064d922/diff",
            "workdir=/var/lib/docker/overlay2/89856b1763c31d022f5161d42069bf069deaaeea98a5c0397069b7a5c064d922/work"
          ],
          "devices": [
            "overlay"
          ]
        },
        "/dev": {
          "kb_size": "65536",
          "kb_used": "0",
          "kb_available": "65536",
          "percent_used": "0%",
          "total_inodes": "2036645",
          "inodes_used": "313",
          "inodes_available": "2036332",
          "inodes_percent_used": "1%",
          "fs_type": "tmpfs",
          "mount_options": [
            "rw",
            "nosuid",
            "size=65536k",
            "mode=755"
          ],
          "devices": [
            "tmpfs"
          ]
        },
        "/sys/fs/cgroup": {
          "kb_size": "8146580",
          "kb_used": "0",
          "kb_available": "8146580",
          "percent_used": "0%",
          "total_inodes": "2036645",
          "inodes_used": "17",
          "inodes_available": "2036628",
          "inodes_percent_used": "1%",
          "fs_type": "tmpfs",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "mode=755"
          ],
          "devices": [
            "tmpfs"
          ]
        },
        "/dev/shm": {
          "kb_size": "65536",
          "kb_used": "0",
          "kb_available": "65536",
          "percent_used": "0%",
          "total_inodes": "2036645",
          "inodes_used": "1",
          "inodes_available": "2036644",
          "inodes_percent_used": "1%",
          "fs_type": "tmpfs",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "size=65536k"
          ],
          "devices": [
            "shm"
          ]
        },
        "/root/.chef": {
          "kb_size": "490691512",
          "kb_used": "98299536",
          "kb_available": "367396496",
          "percent_used": "22%",
          "total_inodes": "31227904",
          "inodes_used": "1582051",
          "inodes_available": "29645853",
          "inodes_percent_used": "6%",
          "fs_type": "ext4",
          "mount_options": [
            "rw",
            "relatime",
            "errors=remount-ro"
          ],
          "uuid": "aa041608-24c8-4ad6-8941-eba955f28642",
          "devices": [
            "/dev/nvme0n1p2"
          ]
        },
        "/run/docker.sock": {
          "kb_size": "1629320",
          "kb_used": "2552",
          "kb_available": "1626768",
          "percent_used": "1%",
          "total_inodes": "2036645",
          "inodes_used": "1325",
          "inodes_available": "2035320",
          "inodes_percent_used": "1%",
          "fs_type": "tmpfs",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "size=1629320k",
            "mode=755"
          ],
          "devices": [
            "tmpfs"
          ]
        },
        "/proc": {
          "fs_type": "proc",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime"
          ],
          "devices": [
            "proc"
          ]
        },
        "/dev/pts": {
          "fs_type": "devpts",
          "mount_options": [
            "rw",
            "nosuid",
            "noexec",
            "relatime",
            "gid=5",
            "mode=620",
            "ptmxmode=666"
          ],
          "devices": [
            "devpts"
          ]
        },
        "/sys": {
          "fs_type": "sysfs",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime"
          ],
          "devices": [
            "sysfs"
          ]
        },
        "/sys/fs/cgroup/systemd": {
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "xattr",
            "name=systemd"
          ],
          "devices": [
            "cgroup"
          ]
        },
        "/sys/fs/cgroup/freezer": {
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "freezer"
          ],
          "devices": [
            "cgroup"
          ]
        },
        "/sys/fs/cgroup/memory": {
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "memory"
          ],
          "devices": [
            "cgroup"
          ]
        },
        "/sys/fs/cgroup/net_cls,net_prio": {
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "net_cls",
            "net_prio"
          ],
          "devices": [
            "cgroup"
          ]
        },
        "/sys/fs/cgroup/perf_event": {
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "perf_event"
          ],
          "devices": [
            "cgroup"
          ]
        },
        "/sys/fs/cgroup/cpuset": {
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "cpuset"
          ],
          "devices": [
            "cgroup"
          ]
        },
        "/sys/fs/cgroup/blkio": {
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "blkio"
          ],
          "devices": [
            "cgroup"
          ]
        },
        "/sys/fs/cgroup/pids": {
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "pids"
          ],
          "devices": [
            "cgroup"
          ]
        },
        "/sys/fs/cgroup/rdma": {
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "rdma"
          ],
          "devices": [
            "cgroup"
          ]
        },
        "/sys/fs/cgroup/hugetlb": {
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "hugetlb"
          ],
          "devices": [
            "cgroup"
          ]
        },
        "/sys/fs/cgroup/devices": {
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "devices"
          ],
          "devices": [
            "cgroup"
          ]
        },
        "/sys/fs/cgroup/cpu,cpuacct": {
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "cpu",
            "cpuacct"
          ],
          "devices": [
            "cgroup"
          ]
        },
        "/dev/mqueue": {
          "fs_type": "mqueue",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime"
          ],
          "devices": [
            "mqueue"
          ]
        },
        "/etc/resolv.conf": {
          "fs_type": "ext4",
          "mount_options": [
            "rw",
            "relatime",
            "errors=remount-ro"
          ],
          "uuid": "aa041608-24c8-4ad6-8941-eba955f28642",
          "devices": [
            "/dev/nvme0n1p2"
          ]
        },
        "/etc/hostname": {
          "fs_type": "ext4",
          "mount_options": [
            "rw",
            "relatime",
            "errors=remount-ro"
          ],
          "uuid": "aa041608-24c8-4ad6-8941-eba955f28642",
          "devices": [
            "/dev/nvme0n1p2"
          ]
        },
        "/etc/hosts": {
          "fs_type": "ext4",
          "mount_options": [
            "rw",
            "relatime",
            "errors=remount-ro"
          ],
          "uuid": "aa041608-24c8-4ad6-8941-eba955f28642",
          "devices": [
            "/dev/nvme0n1p2"
          ]
        },
        "/var/lib/jenkins": {
          "fs_type": "ext4",
          "mount_options": [
            "rw",
            "relatime",
            "errors=remount-ro"
          ],
          "uuid": "aa041608-24c8-4ad6-8941-eba955f28642",
          "devices": [
            "/dev/nvme0n1p2"
          ]
        }
      },
      "by_pair": {
        "overlay,/": {
          "device": "overlay",
          "kb_size": "490691512",
          "kb_used": "98299536",
          "kb_available": "367396496",
          "percent_used": "22%",
          "mount": "/",
          "total_inodes": "31227904",
          "inodes_used": "1582051",
          "inodes_available": "29645853",
          "inodes_percent_used": "6%",
          "fs_type": "overlay",
          "mount_options": [
            "rw",
            "relatime",
            "lowerdir=/var/lib/docker/overlay2/l/6SHGEXVZPDKCO7WUU6IDNJXYXS:/var/lib/docker/overlay2/l/YO3CCII6IESV7DRDND7KBM6QGB:/var/lib/docker/overlay2/l/LWRRY7A33VLU4ISZ5AEX2OUBJ2:/var/lib/docker/overlay2/l/NZXYLZOKAFYWESVSBUHRZISN4F:/var/lib/docker/overlay2/l/K2ZYSGV74DW4RNH2BTKYM6BH2X:/var/lib/docker/overlay2/l/PG2ZTNFPPPQIX4NBVUDJA3XUXA:/var/lib/docker/overlay2/l/XSDYFDIGGDNG75BIDUYOX5RHJD:/var/lib/docker/overlay2/l/JMS47CCCE5CVS5AZSDJQ7QFZQT:/var/lib/docker/overlay2/l/G7QPRQ3QUQWRBNRB7PNSX2VPEM:/var/lib/docker/overlay2/l/6ZTQCWYCBLUOUHLIQC3BNYXTYZ:/var/lib/docker/overlay2/l/5HJOTERUPUHWNMASDCWPQW5JD3:/var/lib/docker/overlay2/l/75YFVNFSF7XJDZHOHCS7NHI5R3:/var/lib/docker/overlay2/l/YBEE3POARI5YV326AVMRBYYRYD:/var/lib/docker/overlay2/l/R3DBMMCSVLOQAQGQP7DP5HKJ4Y",
            "upperdir=/var/lib/docker/overlay2/89856b1763c31d022f5161d42069bf069deaaeea98a5c0397069b7a5c064d922/diff",
            "workdir=/var/lib/docker/overlay2/89856b1763c31d022f5161d42069bf069deaaeea98a5c0397069b7a5c064d922/work"
          ]
        },
        "tmpfs,/dev": {
          "device": "tmpfs",
          "kb_size": "65536",
          "kb_used": "0",
          "kb_available": "65536",
          "percent_used": "0%",
          "mount": "/dev",
          "total_inodes": "2036645",
          "inodes_used": "313",
          "inodes_available": "2036332",
          "inodes_percent_used": "1%",
          "fs_type": "tmpfs",
          "mount_options": [
            "rw",
            "nosuid",
            "size=65536k",
            "mode=755"
          ]
        },
        "tmpfs,/sys/fs/cgroup": {
          "device": "tmpfs",
          "kb_size": "8146580",
          "kb_used": "0",
          "kb_available": "8146580",
          "percent_used": "0%",
          "mount": "/sys/fs/cgroup",
          "total_inodes": "2036645",
          "inodes_used": "17",
          "inodes_available": "2036628",
          "inodes_percent_used": "1%",
          "fs_type": "tmpfs",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "mode=755"
          ]
        },
        "shm,/dev/shm": {
          "device": "shm",
          "kb_size": "65536",
          "kb_used": "0",
          "kb_available": "65536",
          "percent_used": "0%",
          "mount": "/dev/shm",
          "total_inodes": "2036645",
          "inodes_used": "1",
          "inodes_available": "2036644",
          "inodes_percent_used": "1%",
          "fs_type": "tmpfs",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "size=65536k"
          ]
        },
        "/dev/nvme0n1p2,/root/.chef": {
          "device": "/dev/nvme0n1p2",
          "kb_size": "490691512",
          "kb_used": "98299536",
          "kb_available": "367396496",
          "percent_used": "22%",
          "mount": "/root/.chef",
          "total_inodes": "31227904",
          "inodes_used": "1582051",
          "inodes_available": "29645853",
          "inodes_percent_used": "6%",
          "fs_type": "ext4",
          "mount_options": [
            "rw",
            "relatime",
            "errors=remount-ro"
          ],
          "uuid": "aa041608-24c8-4ad6-8941-eba955f28642"
        },
        "tmpfs,/run/docker.sock": {
          "device": "tmpfs",
          "kb_size": "1629320",
          "kb_used": "2552",
          "kb_available": "1626768",
          "percent_used": "1%",
          "mount": "/run/docker.sock",
          "total_inodes": "2036645",
          "inodes_used": "1325",
          "inodes_available": "2035320",
          "inodes_percent_used": "1%",
          "fs_type": "tmpfs",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "size=1629320k",
            "mode=755"
          ]
        },
        "proc,/proc": {
          "device": "proc",
          "mount": "/proc",
          "fs_type": "proc",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime"
          ]
        },
        "devpts,/dev/pts": {
          "device": "devpts",
          "mount": "/dev/pts",
          "fs_type": "devpts",
          "mount_options": [
            "rw",
            "nosuid",
            "noexec",
            "relatime",
            "gid=5",
            "mode=620",
            "ptmxmode=666"
          ]
        },
        "sysfs,/sys": {
          "device": "sysfs",
          "mount": "/sys",
          "fs_type": "sysfs",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime"
          ]
        },
        "cgroup,/sys/fs/cgroup/systemd": {
          "device": "cgroup",
          "mount": "/sys/fs/cgroup/systemd",
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "xattr",
            "name=systemd"
          ]
        },
        "cgroup,/sys/fs/cgroup/freezer": {
          "device": "cgroup",
          "mount": "/sys/fs/cgroup/freezer",
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "freezer"
          ]
        },
        "cgroup,/sys/fs/cgroup/memory": {
          "device": "cgroup",
          "mount": "/sys/fs/cgroup/memory",
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "memory"
          ]
        },
        "cgroup,/sys/fs/cgroup/net_cls,net_prio": {
          "device": "cgroup",
          "mount": "/sys/fs/cgroup/net_cls,net_prio",
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "net_cls",
            "net_prio"
          ]
        },
        "cgroup,/sys/fs/cgroup/perf_event": {
          "device": "cgroup",
          "mount": "/sys/fs/cgroup/perf_event",
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "perf_event"
          ]
        },
        "cgroup,/sys/fs/cgroup/cpuset": {
          "device": "cgroup",
          "mount": "/sys/fs/cgroup/cpuset",
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "cpuset"
          ]
        },
        "cgroup,/sys/fs/cgroup/blkio": {
          "device": "cgroup",
          "mount": "/sys/fs/cgroup/blkio",
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "blkio"
          ]
        },
        "cgroup,/sys/fs/cgroup/pids": {
          "device": "cgroup",
          "mount": "/sys/fs/cgroup/pids",
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "pids"
          ]
        },
        "cgroup,/sys/fs/cgroup/rdma": {
          "device": "cgroup",
          "mount": "/sys/fs/cgroup/rdma",
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "rdma"
          ]
        },
        "cgroup,/sys/fs/cgroup/hugetlb": {
          "device": "cgroup",
          "mount": "/sys/fs/cgroup/hugetlb",
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "hugetlb"
          ]
        },
        "cgroup,/sys/fs/cgroup/devices": {
          "device": "cgroup",
          "mount": "/sys/fs/cgroup/devices",
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "devices"
          ]
        },
        "cgroup,/sys/fs/cgroup/cpu,cpuacct": {
          "device": "cgroup",
          "mount": "/sys/fs/cgroup/cpu,cpuacct",
          "fs_type": "cgroup",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime",
            "cpu",
            "cpuacct"
          ]
        },
        "mqueue,/dev/mqueue": {
          "device": "mqueue",
          "mount": "/dev/mqueue",
          "fs_type": "mqueue",
          "mount_options": [
            "rw",
            "nosuid",
            "nodev",
            "noexec",
            "relatime"
          ]
        },
        "/dev/nvme0n1p2,/etc/resolv.conf": {
          "device": "/dev/nvme0n1p2",
          "mount": "/etc/resolv.conf",
          "fs_type": "ext4",
          "mount_options": [
            "rw",
            "relatime",
            "errors=remount-ro"
          ],
          "uuid": "aa041608-24c8-4ad6-8941-eba955f28642"
        },
        "/dev/nvme0n1p2,/etc/hostname": {
          "device": "/dev/nvme0n1p2",
          "mount": "/etc/hostname",
          "fs_type": "ext4",
          "mount_options": [
            "rw",
            "relatime",
            "errors=remount-ro"
          ],
          "uuid": "aa041608-24c8-4ad6-8941-eba955f28642"
        },
        "/dev/nvme0n1p2,/etc/hosts": {
          "device": "/dev/nvme0n1p2",
          "mount": "/etc/hosts",
          "fs_type": "ext4",
          "mount_options": [
            "rw",
            "relatime",
            "errors=remount-ro"
          ],
          "uuid": "aa041608-24c8-4ad6-8941-eba955f28642"
        },
        "/dev/nvme0n1p2,/var/lib/jenkins": {
          "device": "/dev/nvme0n1p2",
          "mount": "/var/lib/jenkins",
          "fs_type": "ext4",
          "mount_options": [
            "rw",
            "relatime",
            "errors=remount-ro"
          ],
          "uuid": "aa041608-24c8-4ad6-8941-eba955f28642"
        },
        "/dev/loop0,": {
          "device": "/dev/loop0",
          "fs_type": "squashfs"
        },
        "/dev/loop1,": {
          "device": "/dev/loop1",
          "fs_type": "squashfs"
        },
        "/dev/loop2,": {
          "device": "/dev/loop2",
          "fs_type": "squashfs"
        },
        "/dev/loop3,": {
          "device": "/dev/loop3",
          "fs_type": "squashfs"
        },
        "/dev/loop4,": {
          "device": "/dev/loop4",
          "fs_type": "squashfs"
        },
        "/dev/loop5,": {
          "device": "/dev/loop5",
          "fs_type": "squashfs"
        },
        "/dev/loop6,": {
          "device": "/dev/loop6",
          "fs_type": "squashfs"
        },
        "/dev/loop7,": {
          "device": "/dev/loop7",
          "fs_type": "squashfs"
        },
        "/dev/loop8,": {
          "device": "/dev/loop8",
          "fs_type": "squashfs"
        },
        "/dev/loop9,": {
          "device": "/dev/loop9",
          "fs_type": "squashfs"
        },
        "/dev/loop10,": {
          "device": "/dev/loop10",
          "fs_type": "squashfs"
        },
        "/dev/loop11,": {
          "device": "/dev/loop11",
          "fs_type": "squashfs"
        },
        "/dev/loop12,": {
          "device": "/dev/loop12",
          "fs_type": "squashfs"
        },
        "/dev/nvme0n1,": {
          "device": "/dev/nvme0n1"
        },
        "/dev/nvme0n1p1,": {
          "device": "/dev/nvme0n1p1",
          "fs_type": "vfat",
          "uuid": "BE71-571F"
        }
      }
    },
    "dmi": {

    },
    "memory": {
      "swap": {
        "cached": "44728kB",
        "total": "2097148kB",
        "free": "465108kB"
      },
      "hugepages": {
        "total": "0",
        "free": "0",
        "reserved": "0",
        "surplus": "0"
      },
      "total": "16293164kB",
      "free": "9134096kB",
      "available": "11271316kB",
      "buffers": "376944kB",
      "cached": "2231848kB",
      "active": "4763592kB",
      "inactive": "1512984kB",
      "dirty": "333864kB",
      "writeback": "0kB",
      "anon_pages": "3624488kB",
      "mapped": "809076kB",
      "slab": "619760kB",
      "slab_reclaimable": "332828kB",
      "slab_unreclaim": "286932kB",
      "page_tables": "50888kB",
      "nfs_unstable": "0kB",
      "bounce": "0kB",
      "commit_limit": "10243728kB",
      "committed_as": "22080772kB",
      "vmalloc_total": "34359738367kB",
      "vmalloc_used": "88784kB",
      "vmalloc_chunk": "0kB",
      "hugepage_size": "2048kB"
    },
    "lsb": {
      "id": "Ubuntu",
      "description": "Ubuntu 20.04.2 LTS",
      "release": "20.04",
      "codename": "focal"
    },
    "os": "linux",
    "os_version": "5.8.0-53-generic",
    "platform": "ubuntu",
    "platform_version": "20.04",
    "platform_family": "debian",
    "cpu": {
      "0": {
        "vendor_id": "GenuineIntel",
        "family": "6",
        "model": "158",
        "model_name": "Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz",
        "stepping": "10",
        "mhz": "3539.106",
        "cache_size": "9216 KB",
        "physical_id": "0",
        "core_id": "0",
        "cores": "6",
        "flags": [
          "vnmi",
          "preemption_timer",
          "invvpid",
          "ept_x_only",
          "ept_ad",
          "ept_1gb",
          "flexpriority",
          "tsc_offset",
          "vtpr",
          "mtf",
          "vapic",
          "ept",
          "vpid",
          "unrestricted_guest",
          "ple",
          "pml",
          "ept_mode_based_exec"
        ]
      },
      "1": {
        "vendor_id": "GenuineIntel",
        "family": "6",
        "model": "158",
        "model_name": "Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz",
        "stepping": "10",
        "mhz": "3805.434",
        "cache_size": "9216 KB",
        "physical_id": "0",
        "core_id": "1",
        "cores": "6",
        "flags": [
          "vnmi",
          "preemption_timer",
          "invvpid",
          "ept_x_only",
          "ept_ad",
          "ept_1gb",
          "flexpriority",
          "tsc_offset",
          "vtpr",
          "mtf",
          "vapic",
          "ept",
          "vpid",
          "unrestricted_guest",
          "ple",
          "pml",
          "ept_mode_based_exec"
        ]
      },
      "2": {
        "vendor_id": "GenuineIntel",
        "family": "6",
        "model": "158",
        "model_name": "Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz",
        "stepping": "10",
        "mhz": "3657.724",
        "cache_size": "9216 KB",
        "physical_id": "0",
        "core_id": "2",
        "cores": "6",
        "flags": [
          "vnmi",
          "preemption_timer",
          "invvpid",
          "ept_x_only",
          "ept_ad",
          "ept_1gb",
          "flexpriority",
          "tsc_offset",
          "vtpr",
          "mtf",
          "vapic",
          "ept",
          "vpid",
          "unrestricted_guest",
          "ple",
          "pml",
          "ept_mode_based_exec"
        ]
      },
      "3": {
        "vendor_id": "GenuineIntel",
        "family": "6",
        "model": "158",
        "model_name": "Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz",
        "stepping": "10",
        "mhz": "3796.010",
        "cache_size": "9216 KB",
        "physical_id": "0",
        "core_id": "3",
        "cores": "6",
        "flags": [
          "vnmi",
          "preemption_timer",
          "invvpid",
          "ept_x_only",
          "ept_ad",
          "ept_1gb",
          "flexpriority",
          "tsc_offset",
          "vtpr",
          "mtf",
          "vapic",
          "ept",
          "vpid",
          "unrestricted_guest",
          "ple",
          "pml",
          "ept_mode_based_exec"
        ]
      },
      "4": {
        "vendor_id": "GenuineIntel",
        "family": "6",
        "model": "158",
        "model_name": "Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz",
        "stepping": "10",
        "mhz": "3743.842",
        "cache_size": "9216 KB",
        "physical_id": "0",
        "core_id": "4",
        "cores": "6",
        "flags": [
          "vnmi",
          "preemption_timer",
          "invvpid",
          "ept_x_only",
          "ept_ad",
          "ept_1gb",
          "flexpriority",
          "tsc_offset",
          "vtpr",
          "mtf",
          "vapic",
          "ept",
          "vpid",
          "unrestricted_guest",
          "ple",
          "pml",
          "ept_mode_based_exec"
        ]
      },
      "5": {
        "vendor_id": "GenuineIntel",
        "family": "6",
        "model": "158",
        "model_name": "Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz",
        "stepping": "10",
        "mhz": "3748.165",
        "cache_size": "9216 KB",
        "physical_id": "0",
        "core_id": "5",
        "cores": "6",
        "flags": [
          "vnmi",
          "preemption_timer",
          "invvpid",
          "ept_x_only",
          "ept_ad",
          "ept_1gb",
          "flexpriority",
          "tsc_offset",
          "vtpr",
          "mtf",
          "vapic",
          "ept",
          "vpid",
          "unrestricted_guest",
          "ple",
          "pml",
          "ept_mode_based_exec"
        ]
      },
      "6": {
        "vendor_id": "GenuineIntel",
        "family": "6",
        "model": "158",
        "model_name": "Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz",
        "stepping": "10",
        "mhz": "3731.396",
        "cache_size": "9216 KB",
        "physical_id": "0",
        "core_id": "0",
        "cores": "6",
        "flags": [
          "vnmi",
          "preemption_timer",
          "invvpid",
          "ept_x_only",
          "ept_ad",
          "ept_1gb",
          "flexpriority",
          "tsc_offset",
          "vtpr",
          "mtf",
          "vapic",
          "ept",
          "vpid",
          "unrestricted_guest",
          "ple",
          "pml",
          "ept_mode_based_exec"
        ]
      },
      "7": {
        "vendor_id": "GenuineIntel",
        "family": "6",
        "model": "158",
        "model_name": "Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz",
        "stepping": "10",
        "mhz": "3739.502",
        "cache_size": "9216 KB",
        "physical_id": "0",
        "core_id": "1",
        "cores": "6",
        "flags": [
          "vnmi",
          "preemption_timer",
          "invvpid",
          "ept_x_only",
          "ept_ad",
          "ept_1gb",
          "flexpriority",
          "tsc_offset",
          "vtpr",
          "mtf",
          "vapic",
          "ept",
          "vpid",
          "unrestricted_guest",
          "ple",
          "pml",
          "ept_mode_based_exec"
        ]
      },
      "8": {
        "vendor_id": "GenuineIntel",
        "family": "6",
        "model": "158",
        "model_name": "Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz",
        "stepping": "10",
        "mhz": "3778.153",
        "cache_size": "9216 KB",
        "physical_id": "0",
        "core_id": "2",
        "cores": "6",
        "flags": [
          "vnmi",
          "preemption_timer",
          "invvpid",
          "ept_x_only",
          "ept_ad",
          "ept_1gb",
          "flexpriority",
          "tsc_offset",
          "vtpr",
          "mtf",
          "vapic",
          "ept",
          "vpid",
          "unrestricted_guest",
          "ple",
          "pml",
          "ept_mode_based_exec"
        ]
      },
      "9": {
        "vendor_id": "GenuineIntel",
        "family": "6",
        "model": "158",
        "model_name": "Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz",
        "stepping": "10",
        "mhz": "3588.625",
        "cache_size": "9216 KB",
        "physical_id": "0",
        "core_id": "3",
        "cores": "6",
        "flags": [
          "vnmi",
          "preemption_timer",
          "invvpid",
          "ept_x_only",
          "ept_ad",
          "ept_1gb",
          "flexpriority",
          "tsc_offset",
          "vtpr",
          "mtf",
          "vapic",
          "ept",
          "vpid",
          "unrestricted_guest",
          "ple",
          "pml",
          "ept_mode_based_exec"
        ]
      },
      "10": {
        "vendor_id": "GenuineIntel",
        "family": "6",
        "model": "158",
        "model_name": "Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz",
        "stepping": "10",
        "mhz": "3723.685",
        "cache_size": "9216 KB",
        "physical_id": "0",
        "core_id": "4",
        "cores": "6",
        "flags": [
          "vnmi",
          "preemption_timer",
          "invvpid",
          "ept_x_only",
          "ept_ad",
          "ept_1gb",
          "flexpriority",
          "tsc_offset",
          "vtpr",
          "mtf",
          "vapic",
          "ept",
          "vpid",
          "unrestricted_guest",
          "ple",
          "pml",
          "ept_mode_based_exec"
        ]
      },
      "11": {
        "vendor_id": "GenuineIntel",
        "family": "6",
        "model": "158",
        "model_name": "Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz",
        "stepping": "10",
        "mhz": "3778.995",
        "cache_size": "9216 KB",
        "physical_id": "0",
        "core_id": "5",
        "cores": "6",
        "flags": [
          "vnmi",
          "preemption_timer",
          "invvpid",
          "ept_x_only",
          "ept_ad",
          "ept_1gb",
          "flexpriority",
          "tsc_offset",
          "vtpr",
          "mtf",
          "vapic",
          "ept",
          "vpid",
          "unrestricted_guest",
          "ple",
          "pml",
          "ept_mode_based_exec"
        ]
      },
      "total": 12,
      "real": 1,
      "cores": 6
    },
    "keys": {
      "ssh": {
        "host_rsa_public": "AAAAB3NzaC1yc2EAAAADAQABAAABgQDAfRT9QypMMNAFZy1Z4QwEAirBpRIXCD+UZrmCBQ5qLmqwSbIquh1a75p/Tgd5AXePEOYT5f4R8nTDyIG1N9dUxW+qgh67QFakCY3GsES/XmPzBONmSqEDhSUQJI5YVMiuwlLhxXo3klSYCEfxQFwJznDApw61TcAdP3yd0ZRxIPNpDUiffrZ0RxgNdqYDdmu1Wbx67ivmhCjRK36TzLAWg/TtTUvdgbsiMt8xbgTq5keHcXa0psCQQrDJZzvOd9f4bTk2mvryKcmTBk48osLn13zKsNQVjroexXFZFhUxp1IJDCZXaOLbql11WoM92+EjC4TvkW9tAEVpxvnz3aAgKppU7S4rYqebeGfX2+nq1IZ5U7t+7jKsanxkb3vKEPmZWkpUIogxiEDpsHq1IxBSdwiuMW00hN4HjXamndipwe21LGyWNFTl865b4TY9yVATSzFVRR/udUG3qcvDgmfpQ6SS4c4kooHUKb04+TalrbDnc0s4R/eOxvwTM5zfQOc=",
        "host_ecdsa_public": "AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBP1hHCWsZOfD7j9gD2lpgsKTtE/LY7BfzK3zJgsUSnYVMG9I26h01P2JPn34brvvah/K1t+qfTJRdn1AJ4y0ojA=",
        "host_ecdsa_type": "ecdsa-sha2-nistp256",
        "host_ed25519_public": "AAAAC3NzaC1lZDI1NTE5AAAAIFc2Rz3InYhD2DbGSLJD0wdx/S1yyYKryWzDPoHIDiFc"
      }
    },
    "init_package": "run_chef_node.s",
    "uptime_seconds": 43325,
    "uptime": "12 hours 02 minutes 05 seconds",
    "idletime_seconds": 466546,
    "idletime": "5 days 09 hours 35 minutes 46 seconds",
    "root_group": "root",
    "languages": {
      "ruby": {

      },
      "perl": {
        "version": "5.30.0",
        "archname": "x86_64-linux-gnu-thread-multi"
      }
    },
    "sysconf": {
      "LINK_MAX": 127,
      "_POSIX_LINK_MAX": 127,
      "MAX_CANON": 255,
      "_POSIX_MAX_CANON": 255,
      "MAX_INPUT": 255,
      "_POSIX_MAX_INPUT": 255,
      "NAME_MAX": 255,
      "_POSIX_NAME_MAX": 255,
      "PATH_MAX": 4096,
      "_POSIX_PATH_MAX": 4096,
      "PIPE_BUF": 4096,
      "_POSIX_PIPE_BUF": 4096,
      "SOCK_MAXBUF": null,
      "_POSIX_ASYNC_IO": null,
      "_POSIX_CHOWN_RESTRICTED": 1,
      "_POSIX_NO_TRUNC": 1,
      "_POSIX_PRIO_IO": null,
      "_POSIX_SYNC_IO": null,
      "_POSIX_VDISABLE": 0,
      "ARG_MAX": 2097152,
      "ATEXIT_MAX": 2147483647,
      "CHAR_BIT": 8,
      "CHAR_MAX": 127,
      "CHAR_MIN": -128,
      "CHILD_MAX": null,
      "CLK_TCK": 100,
      "INT_MAX": 2147483647,
      "INT_MIN": -2147483648,
      "IOV_MAX": 1024,
      "LOGNAME_MAX": 256,
      "LONG_BIT": 64,
      "MB_LEN_MAX": 16,
      "NGROUPS_MAX": 65536,
      "NL_ARGMAX": 4096,
      "NL_LANGMAX": 2048,
      "NL_MSGMAX": 2147483647,
      "NL_NMAX": 2147483647,
      "NL_SETMAX": 2147483647,
      "NL_TEXTMAX": 2147483647,
      "NSS_BUFLEN_GROUP": 1024,
      "NSS_BUFLEN_PASSWD": 1024,
      "NZERO": 20,
      "OPEN_MAX": 1048576,
      "PAGESIZE": 4096,
      "PAGE_SIZE": 4096,
      "PASS_MAX": 8192,
      "PTHREAD_DESTRUCTOR_ITERATIONS": 4,
      "PTHREAD_KEYS_MAX": 1024,
      "PTHREAD_STACK_MIN": 16384,
      "PTHREAD_THREADS_MAX": null,
      "SCHAR_MAX": 127,
      "SCHAR_MIN": -128,
      "SHRT_MAX": 32767,
      "SHRT_MIN": -32768,
      "SSIZE_MAX": 32767,
      "TTY_NAME_MAX": 32,
      "TZNAME_MAX": null,
      "UCHAR_MAX": 255,
      "UINT_MAX": 4294967295,
      "UIO_MAXIOV": 1024,
      "ULONG_MAX": 18446744073709551615,
      "USHRT_MAX": 65535,
      "WORD_BIT": 32,
      "_AVPHYS_PAGES": 2278324,
      "_NPROCESSORS_CONF": 12,
      "_NPROCESSORS_ONLN": 12,
      "_PHYS_PAGES": 4073291,
      "_POSIX_ARG_MAX": 2097152,
      "_POSIX_ASYNCHRONOUS_IO": 200809,
      "_POSIX_CHILD_MAX": null,
      "_POSIX_FSYNC": 200809,
      "_POSIX_JOB_CONTROL": 1,
      "_POSIX_MAPPED_FILES": 200809,
      "_POSIX_MEMLOCK": 200809,
      "_POSIX_MEMLOCK_RANGE": 200809,
      "_POSIX_MEMORY_PROTECTION": 200809,
      "_POSIX_MESSAGE_PASSING": 200809,
      "_POSIX_NGROUPS_MAX": 65536,
      "_POSIX_OPEN_MAX": 1048576,
      "_POSIX_PII": null,
      "_POSIX_PII_INTERNET": null,
      "_POSIX_PII_INTERNET_DGRAM": null,
      "_POSIX_PII_INTERNET_STREAM": null,
      "_POSIX_PII_OSI": null,
      "_POSIX_PII_OSI_CLTS": null,
      "_POSIX_PII_OSI_COTS": null,
      "_POSIX_PII_OSI_M": null,
      "_POSIX_PII_SOCKET": null,
      "_POSIX_PII_XTI": null,
      "_POSIX_POLL": null,
      "_POSIX_PRIORITIZED_IO": 200809,
      "_POSIX_PRIORITY_SCHEDULING": 200809,
      "_POSIX_REALTIME_SIGNALS": 200809,
      "_POSIX_SAVED_IDS": 1,
      "_POSIX_SELECT": null,
      "_POSIX_SEMAPHORES": 200809,
      "_POSIX_SHARED_MEMORY_OBJECTS": 200809,
      "_POSIX_SSIZE_MAX": 32767,
      "_POSIX_STREAM_MAX": 16,
      "_POSIX_SYNCHRONIZED_IO": 200809,
      "_POSIX_THREADS": 200809,
      "_POSIX_THREAD_ATTR_STACKADDR": 200809,
      "_POSIX_THREAD_ATTR_STACKSIZE": 200809,
      "_POSIX_THREAD_PRIORITY_SCHEDULING": 200809,
      "_POSIX_THREAD_PRIO_INHERIT": 200809,
      "_POSIX_THREAD_PRIO_PROTECT": 200809,
      "_POSIX_THREAD_ROBUST_PRIO_INHERIT": null,
      "_POSIX_THREAD_ROBUST_PRIO_PROTECT": null,
      "_POSIX_THREAD_PROCESS_SHARED": 200809,
      "_POSIX_THREAD_SAFE_FUNCTIONS": 200809,
      "_POSIX_TIMERS": 200809,
      "TIMER_MAX": null,
      "_POSIX_TZNAME_MAX": null,
      "_POSIX_VERSION": 200809,
      "_T_IOV_MAX": null,
      "_XOPEN_CRYPT": null,
      "_XOPEN_ENH_I18N": 1,
      "_XOPEN_LEGACY": 1,
      "_XOPEN_REALTIME": 1,
      "_XOPEN_REALTIME_THREADS": 1,
      "_XOPEN_SHM": 1,
      "_XOPEN_UNIX": 1,
      "_XOPEN_VERSION": 700,
      "_XOPEN_XCU_VERSION": 4,
      "_XOPEN_XPG2": 1,
      "_XOPEN_XPG3": 1,
      "_XOPEN_XPG4": 1,
      "BC_BASE_MAX": 99,
      "BC_DIM_MAX": 2048,
      "BC_SCALE_MAX": 99,
      "BC_STRING_MAX": 1000,
      "CHARCLASS_NAME_MAX": 2048,
      "COLL_WEIGHTS_MAX": 255,
      "EQUIV_CLASS_MAX": null,
      "EXPR_NEST_MAX": 32,
      "LINE_MAX": 2048,
      "POSIX2_BC_BASE_MAX": 99,
      "POSIX2_BC_DIM_MAX": 2048,
      "POSIX2_BC_SCALE_MAX": 99,
      "POSIX2_BC_STRING_MAX": 1000,
      "POSIX2_CHAR_TERM": 200809,
      "POSIX2_COLL_WEIGHTS_MAX": 255,
      "POSIX2_C_BIND": 200809,
      "POSIX2_C_DEV": 200809,
      "POSIX2_C_VERSION": 200809,
      "POSIX2_EXPR_NEST_MAX": 32,
      "POSIX2_FORT_DEV": null,
      "POSIX2_FORT_RUN": null,
      "_POSIX2_LINE_MAX": 2048,
      "POSIX2_LINE_MAX": 2048,
      "POSIX2_LOCALEDEF": 200809,
      "POSIX2_RE_DUP_MAX": 32767,
      "POSIX2_SW_DEV": 200809,
      "POSIX2_UPE": null,
      "POSIX2_VERSION": 200809,
      "RE_DUP_MAX": 32767,
      "PATH": "/bin:/usr/bin",
      "CS_PATH": "/bin:/usr/bin",
      "LFS_CFLAGS": null,
      "LFS_LDFLAGS": null,
      "LFS_LIBS": null,
      "LFS_LINTFLAGS": null,
      "LFS64_CFLAGS": "-D_LARGEFILE64_SOURCE",
      "LFS64_LDFLAGS": null,
      "LFS64_LIBS": null,
      "LFS64_LINTFLAGS": "-D_LARGEFILE64_SOURCE",
      "_XBS5_WIDTH_RESTRICTED_ENVS": "XBS5_LP64_OFF64",
      "XBS5_WIDTH_RESTRICTED_ENVS": "XBS5_LP64_OFF64",
      "_XBS5_ILP32_OFF32": null,
      "XBS5_ILP32_OFF32_CFLAGS": null,
      "XBS5_ILP32_OFF32_LDFLAGS": null,
      "XBS5_ILP32_OFF32_LIBS": null,
      "XBS5_ILP32_OFF32_LINTFLAGS": null,
      "_XBS5_ILP32_OFFBIG": null,
      "XBS5_ILP32_OFFBIG_CFLAGS": null,
      "XBS5_ILP32_OFFBIG_LDFLAGS": null,
      "XBS5_ILP32_OFFBIG_LIBS": null,
      "XBS5_ILP32_OFFBIG_LINTFLAGS": null,
      "_XBS5_LP64_OFF64": 1,
      "XBS5_LP64_OFF64_CFLAGS": "-m64",
      "XBS5_LP64_OFF64_LDFLAGS": "-m64",
      "XBS5_LP64_OFF64_LIBS": null,
      "XBS5_LP64_OFF64_LINTFLAGS": null,
      "_XBS5_LPBIG_OFFBIG": null,
      "XBS5_LPBIG_OFFBIG_CFLAGS": null,
      "XBS5_LPBIG_OFFBIG_LDFLAGS": null,
      "XBS5_LPBIG_OFFBIG_LIBS": null,
      "XBS5_LPBIG_OFFBIG_LINTFLAGS": null,
      "_POSIX_V6_ILP32_OFF32": null,
      "POSIX_V6_ILP32_OFF32_CFLAGS": null,
      "POSIX_V6_ILP32_OFF32_LDFLAGS": null,
      "POSIX_V6_ILP32_OFF32_LIBS": null,
      "POSIX_V6_ILP32_OFF32_LINTFLAGS": null,
      "_POSIX_V6_WIDTH_RESTRICTED_ENVS": "POSIX_V6_LP64_OFF64",
      "POSIX_V6_WIDTH_RESTRICTED_ENVS": "POSIX_V6_LP64_OFF64",
      "_POSIX_V6_ILP32_OFFBIG": null,
      "POSIX_V6_ILP32_OFFBIG_CFLAGS": null,
      "POSIX_V6_ILP32_OFFBIG_LDFLAGS": null,
      "POSIX_V6_ILP32_OFFBIG_LIBS": null,
      "POSIX_V6_ILP32_OFFBIG_LINTFLAGS": null,
      "_POSIX_V6_LP64_OFF64": 1,
      "POSIX_V6_LP64_OFF64_CFLAGS": "-m64",
      "POSIX_V6_LP64_OFF64_LDFLAGS": "-m64",
      "POSIX_V6_LP64_OFF64_LIBS": null,
      "POSIX_V6_LP64_OFF64_LINTFLAGS": null,
      "_POSIX_V6_LPBIG_OFFBIG": null,
      "POSIX_V6_LPBIG_OFFBIG_CFLAGS": null,
      "POSIX_V6_LPBIG_OFFBIG_LDFLAGS": null,
      "POSIX_V6_LPBIG_OFFBIG_LIBS": null,
      "POSIX_V6_LPBIG_OFFBIG_LINTFLAGS": null,
      "_POSIX_V7_ILP32_OFF32": null,
      "POSIX_V7_ILP32_OFF32_CFLAGS": null,
      "POSIX_V7_ILP32_OFF32_LDFLAGS": null,
      "POSIX_V7_ILP32_OFF32_LIBS": null,
      "POSIX_V7_ILP32_OFF32_LINTFLAGS": null,
      "_POSIX_V7_WIDTH_RESTRICTED_ENVS": "POSIX_V7_LP64_OFF64",
      "POSIX_V7_WIDTH_RESTRICTED_ENVS": "POSIX_V7_LP64_OFF64",
      "_POSIX_V7_ILP32_OFFBIG": null,
      "POSIX_V7_ILP32_OFFBIG_CFLAGS": null,
      "POSIX_V7_ILP32_OFFBIG_LDFLAGS": null,
      "POSIX_V7_ILP32_OFFBIG_LIBS": null,
      "POSIX_V7_ILP32_OFFBIG_LINTFLAGS": null,
      "_POSIX_V7_LP64_OFF64": 1,
      "POSIX_V7_LP64_OFF64_CFLAGS": "-m64",
      "POSIX_V7_LP64_OFF64_LDFLAGS": "-m64",
      "POSIX_V7_LP64_OFF64_LIBS": null,
      "POSIX_V7_LP64_OFF64_LINTFLAGS": null,
      "_POSIX_V7_LPBIG_OFFBIG": null,
      "POSIX_V7_LPBIG_OFFBIG_CFLAGS": null,
      "POSIX_V7_LPBIG_OFFBIG_LDFLAGS": null,
      "POSIX_V7_LPBIG_OFFBIG_LIBS": null,
      "POSIX_V7_LPBIG_OFFBIG_LINTFLAGS": null,
      "_POSIX_ADVISORY_INFO": 200809,
      "_POSIX_BARRIERS": 200809,
      "_POSIX_BASE": null,
      "_POSIX_C_LANG_SUPPORT": null,
      "_POSIX_C_LANG_SUPPORT_R": null,
      "_POSIX_CLOCK_SELECTION": 200809,
      "_POSIX_CPUTIME": 200809,
      "_POSIX_THREAD_CPUTIME": 200809,
      "_POSIX_DEVICE_SPECIFIC": null,
      "_POSIX_DEVICE_SPECIFIC_R": null,
      "_POSIX_FD_MGMT": null,
      "_POSIX_FIFO": null,
      "_POSIX_PIPE": null,
      "_POSIX_FILE_ATTRIBUTES": null,
      "_POSIX_FILE_LOCKING": null,
      "_POSIX_FILE_SYSTEM": null,
      "_POSIX_MONOTONIC_CLOCK": 200809,
      "_POSIX_MULTI_PROCESS": null,
      "_POSIX_SINGLE_PROCESS": null,
      "_POSIX_NETWORKING": null,
      "_POSIX_READER_WRITER_LOCKS": 200809,
      "_POSIX_SPIN_LOCKS": 200809,
      "_POSIX_REGEXP": 1,
      "_REGEX_VERSION": null,
      "_POSIX_SHELL": 1,
      "_POSIX_SIGNALS": null,
      "_POSIX_SPAWN": 200809,
      "_POSIX_SPORADIC_SERVER": null,
      "_POSIX_THREAD_SPORADIC_SERVER": null,
      "_POSIX_SYSTEM_DATABASE": null,
      "_POSIX_SYSTEM_DATABASE_R": null,
      "_POSIX_TIMEOUTS": 200809,
      "_POSIX_TYPED_MEMORY_OBJECTS": null,
      "_POSIX_USER_GROUPS": null,
      "_POSIX_USER_GROUPS_R": null,
      "POSIX2_PBS": null,
      "POSIX2_PBS_ACCOUNTING": null,
      "POSIX2_PBS_LOCATE": null,
      "POSIX2_PBS_TRACK": null,
      "POSIX2_PBS_MESSAGE": null,
      "SYMLOOP_MAX": null,
      "STREAM_MAX": 16,
      "AIO_LISTIO_MAX": null,
      "AIO_MAX": null,
      "AIO_PRIO_DELTA_MAX": 20,
      "DELAYTIMER_MAX": 2147483647,
      "HOST_NAME_MAX": 64,
      "LOGIN_NAME_MAX": 256,
      "MQ_OPEN_MAX": null,
      "MQ_PRIO_MAX": 32768,
      "_POSIX_DEVICE_IO": null,
      "_POSIX_TRACE": null,
      "_POSIX_TRACE_EVENT_FILTER": null,
      "_POSIX_TRACE_INHERIT": null,
      "_POSIX_TRACE_LOG": null,
      "RTSIG_MAX": 32,
      "SEM_NSEMS_MAX": null,
      "SEM_VALUE_MAX": 2147483647,
      "SIGQUEUE_MAX": 63414,
      "FILESIZEBITS": 32,
      "POSIX_ALLOC_SIZE_MIN": 4096,
      "POSIX_REC_INCR_XFER_SIZE": null,
      "POSIX_REC_MAX_XFER_SIZE": null,
      "POSIX_REC_MIN_XFER_SIZE": 4096,
      "POSIX_REC_XFER_ALIGN": 4096,
      "SYMLINK_MAX": null,
      "GNU_LIBC_VERSION": "glibc 2.31",
      "GNU_LIBPTHREAD_VERSION": "NPTL 2.31",
      "POSIX2_SYMLINKS": 1,
      "LEVEL1_ICACHE_SIZE": 32768,
      "LEVEL1_ICACHE_ASSOC": 8,
      "LEVEL1_ICACHE_LINESIZE": 64,
      "LEVEL1_DCACHE_SIZE": 32768,
      "LEVEL1_DCACHE_ASSOC": 8,
      "LEVEL1_DCACHE_LINESIZE": 64,
      "LEVEL2_CACHE_SIZE": 262144,
      "LEVEL2_CACHE_ASSOC": 4,
      "LEVEL2_CACHE_LINESIZE": 64,
      "LEVEL3_CACHE_SIZE": 9437184,
      "LEVEL3_CACHE_ASSOC": 12,
      "LEVEL3_CACHE_LINESIZE": 64,
      "LEVEL4_CACHE_SIZE": 0,
      "LEVEL4_CACHE_ASSOC": 0,
      "LEVEL4_CACHE_LINESIZE": 0,
      "IPV6": 200809,
      "RAW_SOCKETS": 200809,
      "_POSIX_IPV6": 200809,
      "_POSIX_RAW_SOCKETS": 200809
    },
    "hostname": "jenkins",
    "machinename": "jenkins",
    "fqdn": "jenkins",
    "domain": null,
    "machine_id": "b748a954487a4ef0b68088a8e18e2453",
    "fips": {
      "kernel": {
        "enabled": false
      }
    },
    "shard_seed": 179714678,
    "chef_packages": {
      "ohai": {
        "version": "14.1.0",
        "ohai_root": "/opt/chef/embedded/lib/ruby/gems/2.5.0/gems/ohai-14.1.0/lib/ohai"
      },
      "chef": {
        "version": "14.1.1",
        "chef_root": "/opt/chef/embedded/lib/ruby/gems/2.5.0/gems/chef-14.1.1/lib"
      }
    },
    "shells": [
      "/bin/sh",
      "/bin/bash",
      "/usr/bin/bash",
      "/bin/rbash",
      "/usr/bin/rbash",
      "/bin/dash",
      "/usr/bin/dash"
    ],
    "time": {
      "timezone": "UTC"
    },
    "ohai_time": 1622696386.0661232,
    "systemd_paths": {
      "temporary": "/tmp",
      "temporary-large": "/var/tmp",
      "system-binaries": "/usr/bin",
      "system-include": "/usr/include",
      "system-library-private": "/usr/lib",
      "system-library-arch": "/usr/lib/x86_64-linux-gnu",
      "system-shared": "/usr/share",
      "system-configuration-factory": "/usr/share/factory/etc",
      "system-state-factory": "/usr/share/factory/var",
      "system-configuration": "/etc",
      "system-runtime": "/run",
      "system-runtime-logs": "/run/log",
      "system-state-private": "/var/lib",
      "system-state-logs": "/var/log",
      "system-state-cache": "/var/cache",
      "system-state-spool": "/var/spool",
      "user-binaries": "/root/.local/bin",
      "user-library-private": "/root/.local/lib",
      "user-library-arch": "/root/.local/lib/x86_64-linux-gnu",
      "user-shared": "/root/.local/share",
      "user-configuration": "/root/.config",
      "user-state-cache": "/root/.cache",
      "user": "/root",
      "user-documents": "/root",
      "user-music": "/root",
      "user-pictures": "/root",
      "user-videos": "/root",
      "user-download": "/root",
      "user-public": "/root",
      "user-templates": "/root",
      "user-desktop": "/root/Desktop",
      "search-binaries": "/usr/local/sbin",
      "search-binaries-default": "/usr/local/sbin",
      "search-library-private": "/root/.local/lib",
      "search-library-arch": "/root/.local/lib/x86_64-linux-gnu",
      "search-shared": "/root/.local/share",
      "search-configuration-factory": "/usr/local/share/factory/etc",
      "search-state-factory": "/usr/local/share/factory/var",
      "search-configuration": "/root/.config"
    },
    "hostnamectl": {

    },
    "block_device": {
      "loop1": {
        "size": "423984",
        "removable": "0",
        "rotational": "0",
        "physical_block_size": "512",
        "logical_block_size": "512"
      },
      "nvme0n1": {
        "size": "1000215216",
        "removable": "0",
        "model": "SAMSUNG MZVLW512HMJP-00000",
        "state": "live",
        "rotational": "0",
        "physical_block_size": "512",
        "logical_block_size": "512"
      },
      "loop8": {
        "size": "132648",
        "removable": "0",
        "rotational": "0",
        "physical_block_size": "512",
        "logical_block_size": "512"
      },
      "loop6": {
        "size": "367752",
        "removable": "0",
        "rotational": "0",
        "physical_block_size": "512",
        "logical_block_size": "512"
      },
      "loop13": {
        "size": "0",
        "removable": "0",
        "rotational": "0",
        "physical_block_size": "512",
        "logical_block_size": "512"
      },
      "loop4": {
        "size": "203112",
        "removable": "0",
        "rotational": "0",
        "physical_block_size": "512",
        "logical_block_size": "512"
      },
      "loop11": {
        "size": "65744",
        "removable": "0",
        "rotational": "0",
        "physical_block_size": "512",
        "logical_block_size": "512"
      },
      "loop2": {
        "size": "113560",
        "removable": "0",
        "rotational": "0",
        "physical_block_size": "512",
        "logical_block_size": "512"
      },
      "loop0": {
        "size": "423984",
        "removable": "0",
        "rotational": "0",
        "physical_block_size": "512",
        "logical_block_size": "512"
      },
      "loop9": {
        "size": "113504",
        "removable": "0",
        "rotational": "0",
        "physical_block_size": "512",
        "logical_block_size": "512"
      },
      "loop7": {
        "size": "202680",
        "removable": "0",
        "rotational": "0",
        "physical_block_size": "512",
        "logical_block_size": "512"
      },
      "loop5": {
        "size": "133320",
        "removable": "0",
        "rotational": "0",
        "physical_block_size": "512",
        "logical_block_size": "512"
      },
      "loop12": {
        "size": "65712",
        "removable": "0",
        "rotational": "0",
        "physical_block_size": "512",
        "logical_block_size": "512"
      },
      "loop3": {
        "size": "448496",
        "removable": "0",
        "rotational": "0",
        "physical_block_size": "512",
        "logical_block_size": "512"
      },
      "loop10": {
        "size": "104536",
        "removable": "0",
        "rotational": "0",
        "physical_block_size": "512",
        "logical_block_size": "512"
      }
    },
    "etc": {
      "passwd": {
        "root": {
          "dir": "/root",
          "gid": 0,
          "uid": 0,
          "shell": "/bin/bash",
          "gecos": "root"
        },
        "daemon": {
          "dir": "/usr/sbin",
          "gid": 1,
          "uid": 1,
          "shell": "/usr/sbin/nologin",
          "gecos": "daemon"
        },
        "bin": {
          "dir": "/bin",
          "gid": 2,
          "uid": 2,
          "shell": "/usr/sbin/nologin",
          "gecos": "bin"
        },
        "sys": {
          "dir": "/dev",
          "gid": 3,
          "uid": 3,
          "shell": "/usr/sbin/nologin",
          "gecos": "sys"
        },
        "sync": {
          "dir": "/bin",
          "gid": 65534,
          "uid": 4,
          "shell": "/bin/sync",
          "gecos": "sync"
        },
        "games": {
          "dir": "/usr/games",
          "gid": 60,
          "uid": 5,
          "shell": "/usr/sbin/nologin",
          "gecos": "games"
        },
        "man": {
          "dir": "/var/cache/man",
          "gid": 12,
          "uid": 6,
          "shell": "/usr/sbin/nologin",
          "gecos": "man"
        },
        "lp": {
          "dir": "/var/spool/lpd",
          "gid": 7,
          "uid": 7,
          "shell": "/usr/sbin/nologin",
          "gecos": "lp"
        },
        "mail": {
          "dir": "/var/mail",
          "gid": 8,
          "uid": 8,
          "shell": "/usr/sbin/nologin",
          "gecos": "mail"
        },
        "news": {
          "dir": "/var/spool/news",
          "gid": 9,
          "uid": 9,
          "shell": "/usr/sbin/nologin",
          "gecos": "news"
        },
        "uucp": {
          "dir": "/var/spool/uucp",
          "gid": 10,
          "uid": 10,
          "shell": "/usr/sbin/nologin",
          "gecos": "uucp"
        },
        "proxy": {
          "dir": "/bin",
          "gid": 13,
          "uid": 13,
          "shell": "/usr/sbin/nologin",
          "gecos": "proxy"
        },
        "www-data": {
          "dir": "/var/www",
          "gid": 33,
          "uid": 33,
          "shell": "/usr/sbin/nologin",
          "gecos": "www-data"
        },
        "backup": {
          "dir": "/var/backups",
          "gid": 34,
          "uid": 34,
          "shell": "/usr/sbin/nologin",
          "gecos": "backup"
        },
        "list": {
          "dir": "/var/list",
          "gid": 38,
          "uid": 38,
          "shell": "/usr/sbin/nologin",
          "gecos": "Mailing List Manager"
        },
        "irc": {
          "dir": "/var/run/ircd",
          "gid": 39,
          "uid": 39,
          "shell": "/usr/sbin/nologin",
          "gecos": "ircd"
        },
        "gnats": {
          "dir": "/var/lib/gnats",
          "gid": 41,
          "uid": 41,
          "shell": "/usr/sbin/nologin",
          "gecos": "Gnats Bug-Reporting System (admin)"
        },
        "nobody": {
          "dir": "/nonexistent",
          "gid": 65534,
          "uid": 65534,
          "shell": "/usr/sbin/nologin",
          "gecos": "nobody"
        },
        "_apt": {
          "dir": "/nonexistent",
          "gid": 65534,
          "uid": 100,
          "shell": "/usr/sbin/nologin",
          "gecos": ""
        },
        "systemd-timesync": {
          "dir": "/run/systemd",
          "gid": 101,
          "uid": 101,
          "shell": "/usr/sbin/nologin",
          "gecos": "systemd Time Synchronization,,,"
        },
        "systemd-network": {
          "dir": "/run/systemd",
          "gid": 103,
          "uid": 102,
          "shell": "/usr/sbin/nologin",
          "gecos": "systemd Network Management,,,"
        },
        "systemd-resolve": {
          "dir": "/run/systemd",
          "gid": 104,
          "uid": 103,
          "shell": "/usr/sbin/nologin",
          "gecos": "systemd Resolver,,,"
        },
        "messagebus": {
          "dir": "/nonexistent",
          "gid": 105,
          "uid": 104,
          "shell": "/usr/sbin/nologin",
          "gecos": ""
        },
        "sshd": {
          "dir": "/run/sshd",
          "gid": 65534,
          "uid": 105,
          "shell": "/usr/sbin/nologin",
          "gecos": ""
        },
        "nodeUser": {
          "dir": "/home/nodeUser",
          "gid": 1000,
          "uid": 1000,
          "shell": "/bin/bash",
          "gecos": ""
        }
      },
      "group": {
        "root": {
          "gid": 0,
          "members": [

          ]
        },
        "daemon": {
          "gid": 1,
          "members": [

          ]
        },
        "bin": {
          "gid": 2,
          "members": [

          ]
        },
        "sys": {
          "gid": 3,
          "members": [

          ]
        },
        "adm": {
          "gid": 4,
          "members": [

          ]
        },
        "tty": {
          "gid": 5,
          "members": [

          ]
        },
        "disk": {
          "gid": 6,
          "members": [

          ]
        },
        "lp": {
          "gid": 7,
          "members": [

          ]
        },
        "mail": {
          "gid": 8,
          "members": [

          ]
        },
        "news": {
          "gid": 9,
          "members": [

          ]
        },
        "uucp": {
          "gid": 10,
          "members": [

          ]
        },
        "man": {
          "gid": 12,
          "members": [

          ]
        },
        "proxy": {
          "gid": 13,
          "members": [

          ]
        },
        "kmem": {
          "gid": 15,
          "members": [

          ]
        },
        "dialout": {
          "gid": 20,
          "members": [

          ]
        },
        "fax": {
          "gid": 21,
          "members": [

          ]
        },
        "voice": {
          "gid": 22,
          "members": [

          ]
        },
        "cdrom": {
          "gid": 24,
          "members": [

          ]
        },
        "floppy": {
          "gid": 25,
          "members": [

          ]
        },
        "tape": {
          "gid": 26,
          "members": [

          ]
        },
        "sudo": {
          "gid": 27,
          "members": [
            "nodeUser"
          ]
        },
        "audio": {
          "gid": 29,
          "members": [

          ]
        },
        "dip": {
          "gid": 30,
          "members": [

          ]
        },
        "www-data": {
          "gid": 33,
          "members": [

          ]
        },
        "backup": {
          "gid": 34,
          "members": [

          ]
        },
        "operator": {
          "gid": 37,
          "members": [

          ]
        },
        "list": {
          "gid": 38,
          "members": [

          ]
        },
        "irc": {
          "gid": 39,
          "members": [

          ]
        },
        "src": {
          "gid": 40,
          "members": [

          ]
        },
        "gnats": {
          "gid": 41,
          "members": [

          ]
        },
        "shadow": {
          "gid": 42,
          "members": [

          ]
        },
        "utmp": {
          "gid": 43,
          "members": [

          ]
        },
        "video": {
          "gid": 44,
          "members": [

          ]
        },
        "sasl": {
          "gid": 45,
          "members": [

          ]
        },
        "plugdev": {
          "gid": 46,
          "members": [

          ]
        },
        "staff": {
          "gid": 50,
          "members": [

          ]
        },
        "games": {
          "gid": 60,
          "members": [

          ]
        },
        "users": {
          "gid": 100,
          "members": [

          ]
        },
        "nogroup": {
          "gid": 65534,
          "members": [

          ]
        },
        "systemd-timesync": {
          "gid": 101,
          "members": [

          ]
        },
        "systemd-journal": {
          "gid": 102,
          "members": [

          ]
        },
        "systemd-network": {
          "gid": 103,
          "members": [

          ]
        },
        "systemd-resolve": {
          "gid": 104,
          "members": [

          ]
        },
        "messagebus": {
          "gid": 105,
          "members": [

          ]
        },
        "ssh": {
          "gid": 106,
          "members": [

          ]
        },
        "nodeUser": {
          "gid": 1000,
          "members": [

          ]
        }
      }
    },
    "current_user": "root",
    "command": {
      "ps": "ps -ef"
    },
    "packages": {
      "adduser": {
        "version": "3.118ubuntu2",
        "arch": "all"
      },
      "apt": {
        "version": "2.0.5",
        "arch": "amd64"
      },
      "base-files": {
        "version": "11ubuntu5.3",
        "arch": "amd64"
      },
      "base-passwd": {
        "version": "3.5.47",
        "arch": "amd64"
      },
      "bash": {
        "version": "5.0-6ubuntu1.1",
        "arch": "amd64"
      },
      "bsdutils": {
        "version": "1:2.34-0.1ubuntu9.1",
        "arch": "amd64"
      },
      "bzip2": {
        "version": "1.0.8-2",
        "arch": "amd64"
      },
      "ca-certificates": {
        "version": "20210119~20.04.1",
        "arch": "all"
      },
      "chef": {
        "version": "14.1.1-1",
        "arch": "amd64"
      },
      "coreutils": {
        "version": "8.30-3ubuntu2",
        "arch": "amd64"
      },
      "dash": {
        "version": "0.5.10.2-6",
        "arch": "amd64"
      },
      "dbus": {
        "version": "1.12.16-2ubuntu2.1",
        "arch": "amd64"
      },
      "debconf": {
        "version": "1.5.73",
        "arch": "all"
      },
      "debianutils": {
        "version": "4.9.1",
        "arch": "amd64"
      },
      "diffutils": {
        "version": "1:3.7-3",
        "arch": "amd64"
      },
      "distro-info-data": {
        "version": "0.43ubuntu1.5",
        "arch": "all"
      },
      "dmsetup": {
        "version": "2:1.02.167-1ubuntu1",
        "arch": "amd64"
      },
      "dpkg": {
        "version": "1.19.7ubuntu3",
        "arch": "amd64"
      },
      "e2fsprogs": {
        "version": "1.45.5-2ubuntu1",
        "arch": "amd64"
      },
      "fdisk": {
        "version": "2.34-0.1ubuntu9.1",
        "arch": "amd64"
      },
      "file": {
        "version": "1:5.38-4",
        "arch": "amd64"
      },
      "findutils": {
        "version": "4.7.0-1ubuntu1",
        "arch": "amd64"
      },
      "gcc-10-base": {
        "version": "10.2.0-5ubuntu1~20.04",
        "arch": "amd64"
      },
      "gir1.2-glib-2.0": {
        "version": "1.64.1-1~ubuntu20.04.1",
        "arch": "amd64"
      },
      "gpgv": {
        "version": "2.2.19-3ubuntu2.1",
        "arch": "amd64"
      },
      "grep": {
        "version": "3.4-1",
        "arch": "amd64"
      },
      "gzip": {
        "version": "1.10-0ubuntu4",
        "arch": "amd64"
      },
      "hostname": {
        "version": "3.23",
        "arch": "amd64"
      },
      "init-system-helpers": {
        "version": "1.57",
        "arch": "all"
      },
      "krb5-locales": {
        "version": "1.17-6ubuntu4.1",
        "arch": "all"
      },
      "libacl1": {
        "version": "2.2.53-6",
        "arch": "amd64"
      },
      "libapparmor1": {
        "version": "2.13.3-7ubuntu5.1",
        "arch": "amd64"
      },
      "libapt-pkg6.0": {
        "version": "2.0.5",
        "arch": "amd64"
      },
      "libargon2-1": {
        "version": "0~20171227-0.2",
        "arch": "amd64"
      },
      "libattr1": {
        "version": "1:2.4.48-5",
        "arch": "amd64"
      },
      "libaudit-common": {
        "version": "1:2.8.5-2ubuntu6",
        "arch": "all"
      },
      "libaudit1": {
        "version": "1:2.8.5-2ubuntu6",
        "arch": "amd64"
      },
      "libblkid1": {
        "version": "2.34-0.1ubuntu9.1",
        "arch": "amd64"
      },
      "libbsd0": {
        "version": "0.10.0-1",
        "arch": "amd64"
      },
      "libbz2-1.0": {
        "version": "1.0.8-2",
        "arch": "amd64"
      },
      "libc-bin": {
        "version": "2.31-0ubuntu9.2",
        "arch": "amd64"
      },
      "libc6": {
        "version": "2.31-0ubuntu9.2",
        "arch": "amd64"
      },
      "libcap-ng0": {
        "version": "0.7.9-2.1build1",
        "arch": "amd64"
      },
      "libcap2": {
        "version": "1:2.32-1",
        "arch": "amd64"
      },
      "libcbor0.6": {
        "version": "0.6.0-0ubuntu1",
        "arch": "amd64"
      },
      "libcom-err2": {
        "version": "1.45.5-2ubuntu1",
        "arch": "amd64"
      },
      "libcrypt1": {
        "version": "1:4.4.10-10ubuntu4",
        "arch": "amd64"
      },
      "libcryptsetup12": {
        "version": "2:2.2.2-3ubuntu2.3",
        "arch": "amd64"
      },
      "libdb5.3": {
        "version": "5.3.28+dfsg1-0.6ubuntu2",
        "arch": "amd64"
      },
      "libdbus-1-3": {
        "version": "1.12.16-2ubuntu2.1",
        "arch": "amd64"
      },
      "libdebconfclient0": {
        "version": "0.251ubuntu1",
        "arch": "amd64"
      },
      "libdevmapper1.02.1": {
        "version": "2:1.02.167-1ubuntu1",
        "arch": "amd64"
      },
      "libedit2": {
        "version": "3.1-20191231-1",
        "arch": "amd64"
      },
      "libexpat1": {
        "version": "2.2.9-1build1",
        "arch": "amd64"
      },
      "libext2fs2": {
        "version": "1.45.5-2ubuntu1",
        "arch": "amd64"
      },
      "libfdisk1": {
        "version": "2.34-0.1ubuntu9.1",
        "arch": "amd64"
      },
      "libffi7": {
        "version": "3.3-4",
        "arch": "amd64"
      },
      "libfido2-1": {
        "version": "1.3.1-1ubuntu2",
        "arch": "amd64"
      },
      "libgcc-s1": {
        "version": "10.2.0-5ubuntu1~20.04",
        "arch": "amd64"
      },
      "libgcrypt20": {
        "version": "1.8.5-5ubuntu1",
        "arch": "amd64"
      },
      "libgirepository-1.0-1": {
        "version": "1.64.1-1~ubuntu20.04.1",
        "arch": "amd64"
      },
      "libglib2.0-0": {
        "version": "2.64.6-1~ubuntu20.04.3",
        "arch": "amd64"
      },
      "libglib2.0-data": {
        "version": "2.64.6-1~ubuntu20.04.3",
        "arch": "all"
      },
      "libgmp10": {
        "version": "2:6.2.0+dfsg-4",
        "arch": "amd64"
      },
      "libgnutls30": {
        "version": "3.6.13-2ubuntu1.3",
        "arch": "amd64"
      },
      "libgpg-error0": {
        "version": "1.37-1",
        "arch": "amd64"
      },
      "libgssapi-krb5-2": {
        "version": "1.17-6ubuntu4.1",
        "arch": "amd64"
      },
      "libhogweed5": {
        "version": "3.5.1+really3.5.1-2ubuntu0.1",
        "arch": "amd64"
      },
      "libicu66": {
        "version": "66.1-2ubuntu2",
        "arch": "amd64"
      },
      "libidn2-0": {
        "version": "2.2.0-2",
        "arch": "amd64"
      },
      "libip4tc2": {
        "version": "1.8.4-3ubuntu2",
        "arch": "amd64"
      },
      "libjson-c4": {
        "version": "0.13.1+dfsg-7ubuntu0.3",
        "arch": "amd64"
      },
      "libk5crypto3": {
        "version": "1.17-6ubuntu4.1",
        "arch": "amd64"
      },
      "libkeyutils1": {
        "version": "1.6-6ubuntu1",
        "arch": "amd64"
      },
      "libkmod2": {
        "version": "27-1ubuntu2",
        "arch": "amd64"
      },
      "libkrb5-3": {
        "version": "1.17-6ubuntu4.1",
        "arch": "amd64"
      },
      "libkrb5support0": {
        "version": "1.17-6ubuntu4.1",
        "arch": "amd64"
      },
      "liblz4-1": {
        "version": "1.9.2-2",
        "arch": "amd64"
      },
      "liblzma5": {
        "version": "5.2.4-1ubuntu1",
        "arch": "amd64"
      },
      "libmagic-mgc": {
        "version": "1:5.38-4",
        "arch": "amd64"
      },
      "libmagic1": {
        "version": "1:5.38-4",
        "arch": "amd64"
      },
      "libmount1": {
        "version": "2.34-0.1ubuntu9.1",
        "arch": "amd64"
      },
      "libmpdec2": {
        "version": "2.4.2-3",
        "arch": "amd64"
      },
      "libncurses6": {
        "version": "6.2-0ubuntu2",
        "arch": "amd64"
      },
      "libncursesw6": {
        "version": "6.2-0ubuntu2",
        "arch": "amd64"
      },
      "libnettle7": {
        "version": "3.5.1+really3.5.1-2ubuntu0.1",
        "arch": "amd64"
      },
      "libnss-systemd": {
        "version": "245.4-4ubuntu3.6",
        "arch": "amd64"
      },
      "libp11-kit0": {
        "version": "0.23.20-1ubuntu0.1",
        "arch": "amd64"
      },
      "libpam-modules": {
        "version": "1.3.1-5ubuntu4.1",
        "arch": "amd64"
      },
      "libpam-modules-bin": {
        "version": "1.3.1-5ubuntu4.1",
        "arch": "amd64"
      },
      "libpam-runtime": {
        "version": "1.3.1-5ubuntu4.1",
        "arch": "all"
      },
      "libpam-systemd": {
        "version": "245.4-4ubuntu3.6",
        "arch": "amd64"
      },
      "libpam0g": {
        "version": "1.3.1-5ubuntu4.1",
        "arch": "amd64"
      },
      "libpcre2-8-0": {
        "version": "10.34-7",
        "arch": "amd64"
      },
      "libpcre3": {
        "version": "2:8.39-12build1",
        "arch": "amd64"
      },
      "libprocps8": {
        "version": "2:3.3.16-1ubuntu2.1",
        "arch": "amd64"
      },
      "libpsl5": {
        "version": "0.21.0-1ubuntu1",
        "arch": "amd64"
      },
      "libpython3-stdlib": {
        "version": "3.8.2-0ubuntu2",
        "arch": "amd64"
      },
      "libpython3.8-minimal": {
        "version": "3.8.5-1~20.04.2",
        "arch": "amd64"
      },
      "libpython3.8-stdlib": {
        "version": "3.8.5-1~20.04.2",
        "arch": "amd64"
      },
      "libreadline8": {
        "version": "8.0-4",
        "arch": "amd64"
      },
      "libseccomp2": {
        "version": "2.5.1-1ubuntu1~20.04.1",
        "arch": "amd64"
      },
      "libselinux1": {
        "version": "3.0-1build2",
        "arch": "amd64"
      },
      "libsemanage-common": {
        "version": "3.0-1build2",
        "arch": "all"
      },
      "libsemanage1": {
        "version": "3.0-1build2",
        "arch": "amd64"
      },
      "libsepol1": {
        "version": "3.0-1",
        "arch": "amd64"
      },
      "libsmartcols1": {
        "version": "2.34-0.1ubuntu9.1",
        "arch": "amd64"
      },
      "libsqlite3-0": {
        "version": "3.31.1-4ubuntu0.2",
        "arch": "amd64"
      },
      "libss2": {
        "version": "1.45.5-2ubuntu1",
        "arch": "amd64"
      },
      "libssl1.1": {
        "version": "1.1.1f-1ubuntu2.3",
        "arch": "amd64"
      },
      "libstdc++6": {
        "version": "10.2.0-5ubuntu1~20.04",
        "arch": "amd64"
      },
      "libsystemd0": {
        "version": "245.4-4ubuntu3.6",
        "arch": "amd64"
      },
      "libtasn1-6": {
        "version": "4.16.0-2",
        "arch": "amd64"
      },
      "libtinfo6": {
        "version": "6.2-0ubuntu2",
        "arch": "amd64"
      },
      "libudev1": {
        "version": "245.4-4ubuntu3.6",
        "arch": "amd64"
      },
      "libunistring2": {
        "version": "0.9.10-2",
        "arch": "amd64"
      },
      "libuuid1": {
        "version": "2.34-0.1ubuntu9.1",
        "arch": "amd64"
      },
      "libwrap0": {
        "version": "7.6.q-30",
        "arch": "amd64"
      },
      "libx11-6": {
        "version": "2:1.6.9-2ubuntu1.1",
        "arch": "amd64"
      },
      "libx11-data": {
        "version": "2:1.6.9-2ubuntu1.1",
        "arch": "all"
      },
      "libxau6": {
        "version": "1:1.0.9-0ubuntu1",
        "arch": "amd64"
      },
      "libxcb1": {
        "version": "1.14-2",
        "arch": "amd64"
      },
      "libxdmcp6": {
        "version": "1:1.1.3-0ubuntu1",
        "arch": "amd64"
      },
      "libxext6": {
        "version": "2:1.3.4-0ubuntu1",
        "arch": "amd64"
      },
      "libxml2": {
        "version": "2.9.10+dfsg-5",
        "arch": "amd64"
      },
      "libxmuu1": {
        "version": "2:1.1.3-0ubuntu1",
        "arch": "amd64"
      },
      "libzstd1": {
        "version": "1.4.4+dfsg-3ubuntu0.1",
        "arch": "amd64"
      },
      "login": {
        "version": "1:4.8.1-1ubuntu5.20.04",
        "arch": "amd64"
      },
      "logsave": {
        "version": "1.45.5-2ubuntu1",
        "arch": "amd64"
      },
      "lsb-base": {
        "version": "11.1.0ubuntu2",
        "arch": "all"
      },
      "lsb-release": {
        "version": "11.1.0ubuntu2",
        "arch": "all"
      },
      "mawk": {
        "version": "1.3.4.20200120-2",
        "arch": "amd64"
      },
      "mime-support": {
        "version": "3.64ubuntu1",
        "arch": "all"
      },
      "mount": {
        "version": "2.34-0.1ubuntu9.1",
        "arch": "amd64"
      },
      "ncurses-base": {
        "version": "6.2-0ubuntu2",
        "arch": "all"
      },
      "ncurses-bin": {
        "version": "6.2-0ubuntu2",
        "arch": "amd64"
      },
      "ncurses-term": {
        "version": "6.2-0ubuntu2",
        "arch": "all"
      },
      "netcat": {
        "version": "1.206-1ubuntu1",
        "arch": "all"
      },
      "netcat-openbsd": {
        "version": "1.206-1ubuntu1",
        "arch": "amd64"
      },
      "networkd-dispatcher": {
        "version": "2.0.1-1",
        "arch": "all"
      },
      "openssh-client": {
        "version": "1:8.2p1-4ubuntu0.2",
        "arch": "amd64"
      },
      "openssh-server": {
        "version": "1:8.2p1-4ubuntu0.2",
        "arch": "amd64"
      },
      "openssh-sftp-server": {
        "version": "1:8.2p1-4ubuntu0.2",
        "arch": "amd64"
      },
      "openssl": {
        "version": "1.1.1f-1ubuntu2.3",
        "arch": "amd64"
      },
      "passwd": {
        "version": "1:4.8.1-1ubuntu5.20.04",
        "arch": "amd64"
      },
      "perl-base": {
        "version": "5.30.0-9ubuntu0.2",
        "arch": "amd64"
      },
      "procps": {
        "version": "2:3.3.16-1ubuntu2.1",
        "arch": "amd64"
      },
      "publicsuffix": {
        "version": "20200303.0012-1",
        "arch": "all"
      },
      "python3": {
        "version": "3.8.2-0ubuntu2",
        "arch": "amd64"
      },
      "python3-certifi": {
        "version": "2019.11.28-1",
        "arch": "all"
      },
      "python3-chardet": {
        "version": "3.0.4-4build1",
        "arch": "all"
      },
      "python3-dbus": {
        "version": "1.2.16-1build1",
        "arch": "amd64"
      },
      "python3-distro": {
        "version": "1.4.0-1",
        "arch": "all"
      },
      "python3-gi": {
        "version": "3.36.0-1",
        "arch": "amd64"
      },
      "python3-idna": {
        "version": "2.8-1",
        "arch": "all"
      },
      "python3-minimal": {
        "version": "3.8.2-0ubuntu2",
        "arch": "amd64"
      },
      "python3-pkg-resources": {
        "version": "45.2.0-1",
        "arch": "all"
      },
      "python3-requests": {
        "version": "2.22.0-2ubuntu1",
        "arch": "all"
      },
      "python3-six": {
        "version": "1.14.0-2",
        "arch": "all"
      },
      "python3-urllib3": {
        "version": "1.25.8-2ubuntu0.1",
        "arch": "all"
      },
      "python3.8": {
        "version": "3.8.5-1~20.04.2",
        "arch": "amd64"
      },
      "python3.8-minimal": {
        "version": "3.8.5-1~20.04.2",
        "arch": "amd64"
      },
      "readline-common": {
        "version": "8.0-4",
        "arch": "all"
      },
      "sed": {
        "version": "4.7-1",
        "arch": "amd64"
      },
      "sensible-utils": {
        "version": "0.0.12+nmu1",
        "arch": "all"
      },
      "shared-mime-info": {
        "version": "1.15-1",
        "arch": "amd64"
      },
      "ssh": {
        "version": "1:8.2p1-4ubuntu0.2",
        "arch": "all"
      },
      "ssh-import-id": {
        "version": "5.10-0ubuntu1",
        "arch": "all"
      },
      "sshpass": {
        "version": "1.06-1",
        "arch": "amd64"
      },
      "sudo": {
        "version": "1.8.31-1ubuntu1.2",
        "arch": "amd64"
      },
      "systemd": {
        "version": "245.4-4ubuntu3.6",
        "arch": "amd64"
      },
      "systemd-sysv": {
        "version": "245.4-4ubuntu3.6",
        "arch": "amd64"
      },
      "systemd-timesyncd": {
        "version": "245.4-4ubuntu3.6",
        "arch": "amd64"
      },
      "sysvinit-utils": {
        "version": "2.96-2.1ubuntu1",
        "arch": "amd64"
      },
      "tar": {
        "version": "1.30+dfsg-7ubuntu0.20.04.1",
        "arch": "amd64"
      },
      "tzdata": {
        "version": "2021a-0ubuntu0.20.04",
        "arch": "all"
      },
      "ubuntu-keyring": {
        "version": "2020.02.11.4",
        "arch": "all"
      },
      "ucf": {
        "version": "3.0038+nmu1",
        "arch": "all"
      },
      "util-linux": {
        "version": "2.34-0.1ubuntu9.1",
        "arch": "amd64"
      },
      "wget": {
        "version": "1.20.3-1ubuntu1",
        "arch": "amd64"
      },
      "xauth": {
        "version": "1:1.1-0ubuntu1",
        "arch": "amd64"
      },
      "xdg-user-dirs": {
        "version": "0.17-2ubuntu1",
        "arch": "amd64"
      },
      "xz-utils": {
        "version": "5.2.4-1ubuntu1",
        "arch": "amd64"
      },
      "zlib1g": {
        "version": "1:1.2.11.dfsg-2ubuntu1.2",
        "arch": "amd64"
      }
    },
    "cloud": null,
    "docker": {

    },
    "chef_guid": "4c155834-34df-4362-822c-7f6a4fa3ab6d",
    "name": "jenkins",
    "chef_environment": "_default",
    "recipes": [

    ],
    "expanded_run_list": [

    ],
    "roles": [

    ]
  },
  "normal": {
    "tags": [

    ]
  },
  "chef_type": "node",
  "default": {

  },
  "override": {

  },
  "run_list": [

  ]
}
