import { enigma_inspec } from './enigma_inspect_run';
import { enigma_raw } from './enigma.raw';
import { nexus_inspec } from './nexus_inspect_run';
import { nexus_raw } from './nexus.raw';
import { jenkins_inspec } from './jenkins_inspect_run';
import { jenkins_raw } from './jenkins.raw';

export const hardCodedSampleData = [
    {
        name: 'enigma',
        latest_inspec_run: {
            result: "noncompliant",
            issues: "2"
        }
    },
    {
        name: 'jenkins',
        latest_inspec_run: {
            result: "compliant",
            issues: "0"
        }
    },
    {
        name: 'nexus',
        latest_inspec_run: {
            result: "noncompliant",
            issues: "2"
        }
    },
];

export const hardCodedData = {
    'enigma': {
        name: 'enigma',
        details: enigma_raw,
        inspecRuns: [enigma_inspec]
    },
    'jenkins': {
        name: 'jenkins',
        details: jenkins_raw,
        inspecRuns: [jenkins_inspec]
    },
    'nexus': {
        name: 'nexus',
        details: nexus_raw,
        inspecRuns: [nexus_inspec]
    }
};