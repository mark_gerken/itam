import { hardCodedData, hardCodedSampleData } from './hard_coded_data';

export const loadFleet = (dispatch, getState) => {
    // TODO: add a rest API call here to the chef-client-manager service
    setTimeout(() => {
        dispatch({
            type: 'LOAD_FLEET',
            value: hardCodedSampleData
        });
    }, 3000);
};

export const loadMachine = (nodeName) => {
    return (dispatch, getState) => {
        dispatch({type: 'UNLOAD_MACHINE'});
        setTimeout(() => {
            dispatch({
                type: "LOAD_MACHINE",
                value: hardCodedData[nodeName]
            });
        }, 3000);
    }
};

export const triggerScan = (machineName) => {
    return (dispatch, getState) => {
        dispatch({
            type: "SCAN_INITIATED",
            value: machineName
        });
        setTimeout(() => {
            dispatch({
                type: "SCAN_COMPLETE",
                value: machineName
            });           
        }, 3000)
    }
}

export const scanAllMachines = (dispatch, getState) => {
    getState().fleet.forEach((machine) => {
        dispatch(triggerScan(machine.name));
    });
}