import { configureStore } from '@reduxjs/toolkit';
import activeScaneReducer from './reducers/active_scans';
import fleetReducer from './reducers/fleet_reducer';
import machineReducer from './reducers/machine_reducer';

export const store = configureStore({
  reducer: {
    fleet: fleetReducer,
    machine: machineReducer,
    activeScans: activeScaneReducer
  }
});
